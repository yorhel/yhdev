# List of all input files. Each file is converted into a .html file at the same path.
#
# The format of each line is: $path $URL $title
#
# If no $URL is given or the $URL is '-', then the input file is assumed to be
# in dat/, otherwise it will be fetched from $URL.
#
# A $title should be given for .pod, .log, .mdoc and .man files, it is ignored for
# .md files because those already have a title embedded in the file.
#
# Supported file types:
#   .md:    Converted directly into .html with pandoc.
#		    (the types below are first converted into .md and then into .html)
#   .log:   A ChangeLog-style file
#   .pod:   Perl's Plain Old Documentation
#   .mdoc:  Mandoc page
#   .man:   Manual page
PAGES=\
	"contributing.md"\
	"doc.md"\
	"doc/commvis.md"\
	"doc/dcstats.md"\
	"doc/easyipc.md"\
	"doc/funcweb.md"\
	"doc/ncdu2.md"\
	"doc/pwlookup.md"\
	"doc/sqlaccess.md"\
	"doc/sqlobject.md"\
	"doc/vndbtitles.md"\
	"download.dl"\
	"dump.md"\
	"dump/awshrink.md"\
	"dump/btrfssize.md"\
	"dump/demo.md"\
	"dump/grenamr.md"\
	"dump/insbench.md"\
	"dump/nccolour.md"\
	"guestbook.md"\
	"guestbook/thanks.md"\
	"globster.md"\
	"globster/api.pod           https://g.blicky.net/globster.git/plain/doc/api.pod             The Globster D-Bus API"\
	"globster/ctl.pod           https://g.blicky.net/globster.git/plain/doc/globsterctl.pod     The globsterctl(1) Man Page"\
	"globster/daemon.pod        https://g.blicky.net/globster.git/plain/doc/globster.pod        The globster(1) Man Page"\
	"globster/launch.pod        https://g.blicky.net/globster.git/plain/doc/globster-launch.pod The globster-launch(1) Man Page"\
	"index.md"\
	"ncdc.md"\
	"ncdc/changes.log           https://g.blicky.net/ncdc.git/plain/ChangeLog                   Ncdc Release History"\
	"ncdc/faq.md"\
	"ncdc/install.md"\
	"ncdc/man.pod               -                                                               Ncdc Manual"\
	"ncdc/scr.md"\
	"ncdu.md"\
	"ncdu/changes.log           https://g.blicky.net/ncdu.git/plain/ChangeLog?h=master          Ncdu 1.x Release History"\
	"ncdu/changes2.log          https://g.blicky.net/ncdu.git/plain/ChangeLog?h=zig             Ncdu 2.x Release History"\
	"ncdu/binfmt.md"\
	"ncdu/jsonfmt.md"\
	"ncdu/man.mdoc              https://g.blicky.net/ncdu.git/plain/ncdu.1?h=v2.8               Ncdu 2.8 Manual"\
	"ncdu/man/2_7.mdoc          https://g.blicky.net/ncdu.git/plain/ncdu.1?h=v2.7               Ncdu 2.7 Manual"\
	"ncdu/man/2_6.mdoc          https://g.blicky.net/ncdu.git/plain/ncdu.1?h=v2.6               Ncdu 2.6 Manual"\
	"ncdu/man/2_5.mdoc          https://g.blicky.net/ncdu.git/plain/ncdu.1?h=v2.5               Ncdu 2.5 Manual"\
	"ncdu/man/2_4.mdoc          https://g.blicky.net/ncdu.git/plain/ncdu.1?h=v2.4               Ncdu 2.4 Manual"\
	"ncdu/man/2_3.pod           https://g.blicky.net/ncdu.git/plain/ncdu.pod?h=v2.3             Ncdu 2.3 Manual"\
	"ncdu/man/2_2.pod           https://g.blicky.net/ncdu.git/plain/ncdu.pod?h=v2.2             Ncdu 2.2 Manual"\
	"ncdu/man/2_1.pod           https://g.blicky.net/ncdu.git/plain/ncdu.pod?h=v2.1             Ncdu 2.1 Manual"\
	"ncdu/man/2_0.pod           https://g.blicky.net/ncdu.git/plain/ncdu.pod?h=v2.0             Ncdu 2.0 Manual"\
	"ncdu/man/1_22.mdoc         https://g.blicky.net/ncdu.git/plain/ncdu.1?h=v1.22              Ncdu 1.22 Manual"\
	"ncdu/man/1_21.mdoc         https://g.blicky.net/ncdu.git/plain/ncdu.1?h=v1.21              Ncdu 1.21 Manual"\
	"ncdu/man/1_20.mdoc         https://g.blicky.net/ncdu.git/plain/ncdu.1?h=v1.20              Ncdu 1.20 Manual"\
	"ncdu/man/1_19.pod          https://g.blicky.net/ncdu.git/plain/doc/ncdu.pod?h=v1.19        Ncdu 1.19 Manual"\
	"ncdu/man/1_18.pod          https://g.blicky.net/ncdu.git/plain/doc/ncdu.pod?h=v1.18        Ncdu 1.18 Manual"\
	"ncdu/man/1_17.pod          https://g.blicky.net/ncdu.git/plain/doc/ncdu.pod?h=v1.17        Ncdu 1.17 Manual"\
	"ncdu/man/1_16.pod          https://g.blicky.net/ncdu.git/plain/doc/ncdu.pod?h=v1.16        Ncdu 1.16 Manual"\
	"ncdu/man/1_15_1.pod        https://g.blicky.net/ncdu.git/plain/doc/ncdu.pod?h=v1.15.1      Ncdu 1.15.1 Manual"\
	"ncdu/man/1_15.pod          https://g.blicky.net/ncdu.git/plain/doc/ncdu.pod?h=v1.15        Ncdu 1.15 Manual"\
	"ncdu/man/1_14.pod          https://g.blicky.net/ncdu.git/plain/doc/ncdu.pod?h=v1.14        Ncdu 1.14 Manual"\
	"ncdu/man/1_13.pod          https://g.blicky.net/ncdu.git/plain/doc/ncdu.pod?h=v1.13        Ncdu 1.13 Manual"\
	"ncdu/man/1_12.pod          https://g.blicky.net/ncdu.git/plain/doc/ncdu.pod?h=v1.12        Ncdu 1.12 Manual"\
	"ncdu/man/1_11.pod          https://g.blicky.net/ncdu.git/plain/doc/ncdu.pod?h=v1.11        Ncdu 1.11 Manual"\
	"ncdu/man/1_10.pod          https://g.blicky.net/ncdu.git/plain/doc/ncdu.pod?h=v1.10        Ncdu 1.10 Manual"\
	"ncdu/man/1_9.pod           https://g.blicky.net/ncdu.git/plain/doc/ncdu.pod?h=v1.9         Ncdu 1.9 Manual"\
	"ncdu/man/1_8.man           https://g.blicky.net/ncdu.git/plain/doc/ncdu.1?h=v1.8           Ncdu 1.8 Manual"\
	"ncdu/man/1_7.man           https://g.blicky.net/ncdu.git/plain/doc/ncdu.1?h=v1.7           Ncdu 1.7 Manual"\
	"ncdu/man/1_6.man           https://g.blicky.net/ncdu.git/plain/doc/ncdu.1?h=v1.6           Ncdu 1.6 Manual"\
	"ncdu/man/1_5.man           https://g.blicky.net/ncdu.git/plain/doc/ncdu.1?h=v1.5           Ncdu 1.5 Manual"\
	"ncdu/man/1_4.man           https://g.blicky.net/ncdu.git/plain/doc/ncdu.1?h=v1.4           Ncdu 1.4 Manual"\
	"ncdu/man/1_3.man           https://g.blicky.net/ncdu.git/plain/doc/ncdu.1?h=v1.3           Ncdu 1.3 Manual"\
	"ncdu/man/1_2.man           https://g.blicky.net/ncdu.git/plain/doc/ncdu.1?h=v1.2           Ncdu 1.2 Manual"\
	"ncdu/man/1_1.man           https://g.blicky.net/ncdu.git/plain/doc/ncdu.1?h=v1.1           Ncdu 1.1 Manual"\
	"ncdu/scr.md"\
	"nginx-confgen.md"\
	"nginx-confgen/changes.log  https://g.blicky.net/nginx-confgen.git/plain/ChangeLog                nginx-confgen Release History"\
	"nginx-confgen/man.mdoc     https://g.blicky.net/nginx-confgen.git/plain/nginx-confgen.1?h=2.2    nginx-confgen 2.2 Manual"\
	"nginx-confgen/man/2.1.pod  https://g.blicky.net/nginx-confgen.git/plain/nginx-confgen.pod?h=2.1  nginx-confgen 2.1 Manual"\
	"nginx-confgen/man/2.0.pod  https://g.blicky.net/nginx-confgen.git/plain/nginx-confgen.pod?h=2.0  nginx-confgen 2.0 Manual"\
	"nginx-confgen/man/1_2.pod  https://g.blicky.net/nginx-confgen.git/plain/nginx-confgen.pod?h=1.2  nginx-confgen 1.2 Manual"\
	"nginx-confgen/man/1_1.pod  https://g.blicky.net/nginx-confgen.git/plain/nginx-confgen.pod?h=1.1  nginx-confgen 1.1 Manual"\
	"nginx-confgen/man/1_0.pod  https://g.blicky.net/nginx-confgen.git/plain/nginx-confgen.pod?h=1.0  nginx-confgen 1.0 Manual"\
	"sqlbin.md"\
	"tuwf.md"\
	"sqlbin/manual.md           https://g.blicky.net/sqlbin.git/plain/manual.md"\
	"tuwf/changes.log           https://g.blicky.net/tuwf.git/plain/ChangeLog                   TUWF Release History"\
	"tuwf/man.pod               https://g.blicky.net/tuwf.git/plain/lib/TUWF.pod                TUWF Documentation"\
	"tuwf/man/db.pod            https://g.blicky.net/tuwf.git/plain/lib/TUWF/DB.pod             TUWF::DB Documentation"\
	"tuwf/man/intro.pod         https://g.blicky.net/tuwf.git/plain/lib/TUWF/Intro.pod          TUWF::Intro Documentation"\
	"tuwf/man/misc.pod          https://g.blicky.net/tuwf.git/plain/lib/TUWF/Misc.pod           TUWF::Misc Documentation"\
	"tuwf/man/request.pod       https://g.blicky.net/tuwf.git/plain/lib/TUWF/Request.pod        TUWF::Request Documentation"\
	"tuwf/man/response.pod      https://g.blicky.net/tuwf.git/plain/lib/TUWF/Response.pod       TUWF::Response Documentation"\
	"tuwf/man/validate.pod      https://g.blicky.net/tuwf.git/plain/lib/TUWF/Validate.pod       TUWF::Validate Documentation"\
	"tuwf/man/xml.pod           https://g.blicky.net/tuwf.git/plain/lib/TUWF/XML.pod            TUWF::XML Documentation"\
	"ylib.pod                   https://g.blicky.net/ylib.git/plain/README.pod                  Ylib"\
	"yxml.md"\
	"yxml/man.md                https://g.blicky.net/yxml.git/plain/yxml.md"\
	"torreas.md"\
	"torreas/changes.log        https://g.blicky.net/torreas.git/plain/ChangeLog                Torreas Release History"\
	"torreas/man.mdoc           https://g.blicky.net/torreas.git/plain/torreas.1                Torreas 0.3 Manual"\
	"torreas/man/0_2.mdoc       https://g.blicky.net/torreas.git/plain/torreas.1?h=0.2          Torreas 0.2 Manual"\
	"torreas/man/0_1.pod        https://g.blicky.net/torreas.git/plain/torreas.pod?h=0.1        Torreas 0.1 Manual"


# Files generated by mkfeed.pl
FEEDS=\
	pub/feed.atom\
	pub/globster/feed.atom\
	pub/ncdc/feed.atom\
	pub/ncdu/feed.atom\
	pub/nginx-confgen/feed.atom\
	pub/tuwf/feed.atom\
	pub/yxml/feed.atom\
	pub/torreas/feed.atom


# Files we need to download
FETCH      := $(shell for i in ${PAGES}; do echo "$$i" | grep -Eo '^[^ ]+ +[^ -][^ ]+' | sed -E 's/^([^ ]+).*/dat\/\1/'; done)

# List of generated .html files
HTML_OUT   := $(shell for i in ${PAGES}; do echo "$$i" | sed -E 's/^([^ ]+)\.[^\. ]+.*$$/pub\/\1.html/'; done)

# List of .md files generated from .pod files
POD_MD     := $(shell for i in ${PAGES}; do echo "$$i" | grep -Eo '^[^ ]+\.pod' | sed -E 's/(.+)\.pod$$/dat\/\1.md/'; done)

# List of .md files generated from .log files
CHANGES_MD := $(shell for i in ${PAGES}; do echo "$$i" | grep -Eo '^[^ ]+\.log' | sed -E 's/(.+)\.log$$/dat\/\1.md/'; done)

# List of .md files generated from .mdoc files
MDOC_MD    := $(shell for i in ${PAGES}; do echo "$$i" | grep -Eo '^[^ ]+\.mdoc' | sed -E 's/(.+)\.mdoc$$/dat\/\1.md/'; done)

# List of .md files generated from .man files
MAN_MD     := $(shell for i in ${PAGES}; do echo "$$i" | grep -Eo '^[^ ]+\.man' | sed -E 's/(.+)\.man$$/dat\/\1.md/'; done)

# All fetched & generated files
CLEAN      := dat/download.md ${FETCH} ${POD_MD} ${CHANGES_MD} ${MDOC_MD} ${MAN_MD} ${HTML_OUT} ${FEEDS}


.PHONY: all clean

all: .gitignore ${HTML_OUT} ${FEEDS}


${FEEDS}: mkfeed.pl dat/index.md
	@echo "FEED   $@"
	@mkdir -p $$(dirname "$@")
	@./mkfeed.pl "$@" <dat/index.md >"$@"


${FETCH}: dat/%:
	@echo "FETCH  $*"
	@mkdir -p $$(dirname "$@")
	@curl --user-agent 'YHDEV' -s ${shell for i in ${PAGES}; do case "$$i" in "$* "*) echo "$$i" | awk '{print$$2}';; esac; done} -o "$@"


${POD_MD}: dat/%.md: dat/%.pod
	@echo "POD    $*"
	@(echo "% ${shell for i in ${PAGES}; do case "$$i" in "$*.pod "*) echo "$$i" | sed -E 's/[^ ]+ +[^ ]+ +//';; esac; done}";\
		sed -E -e 's/^=encoding.+//' $< | pandoc -f pod -t markdown | ./manproc.pl pod ) >"$@"

${CHANGES_MD}: dat/%.md: dat/%.log mkchangelog.pl
	@echo "MD     $*"
	@./mkchangelog.pl "$*" "${shell for i in ${PAGES}; do case "$$i" in "$*.log "*) echo "$$i" | sed -E 's/[^ ]+ +[^ ]+ +//';; esac; done}" <"$<" >"$@"


${MDOC_MD}: dat/%.md: dat/%.mdoc
	@echo "MDOC   $*"
	@(echo "% ${shell for i in ${PAGES}; do case "$$i" in "$*.mdoc "*) echo "$$i" | sed -E 's/[^ ]+ +[^ ]+ +//';; esac; done}";\
		pandoc -f mdoc -t markdown $< | ./manproc.pl mdoc ) >"$@"

${MAN_MD}: dat/%.md: dat/%.man
	@echo "MAN    $*"
	@(echo "% ${shell for i in ${PAGES}; do case "$$i" in "$*.man "*) echo "$$i" | sed -E 's/[^ ]+ +[^ ]+ +//';; esac; done}";\
		pandoc -f man -t markdown $< | ./manproc.pl man ) >"$@"


dat/download.md: mkdlpage.pl pub/download/*.sha256
	./mkdlpage.pl >"$@"


${HTML_OUT}: pub/%.html: dat/%.md template.html Makefile
	@echo "HTML   $*"
	@mkdir -p $$(dirname "$@")
	@cat "$<" |\
		sed -E 's/\[([^]]+)\]\(\/download\/([^\) ]+)\)/\0 <a class="dlinfo" title="File info" href="\/download#\2">ⓘ<\/a>/g' |\
		./docversions.pl "$*" ${HTML_OUT} |\
		pandoc -f markdown -t html5 --strip-comments --template template.html \
			--metadata path1=$$(echo "$*" | sed 's/\/.*//') \
			--metadata path2=$$(echo "$*" | sed 's/\//-/' | sed 's/\/.*//') \
			--metadata path3=$$(echo "$*" | sed 's/\//-/g') \
			--variable menu-$$(case "$*" in\
					globster*)      echo "globster";;\
					ncdc*)          echo "ncdc";;\
					ncdu*)          echo "ncdu";;\
					nginx-confgen*) echo "nginx-confgen";;\
					tuwf*)          echo "tuwf";;\
					yxml*)          echo "yxml";;\
					sqlbin*)        echo "sqlbin";;\
					torreas*)       echo "torreas";;\
					*) echo "main";;\
				esac)\
			-o "$@"


.gitignore: Makefile
	@echo "GIT"
	@echo '*.zip'  >$@
	@echo '*.gz'  >>$@
	@echo '*.pdf' >>$@
	@for i in ${CLEAN}; do echo "$$i"; done | sort >>$@


clean:
	rm -rf ${CLEAN}
	find dat pub -type d -empty -print -delete
