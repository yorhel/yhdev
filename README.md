This is the source code of the site running at [dev.yorhel.nl](https://dev.yorhel.nl/).

# Requirements

Build-time:

- GNU Make
- curl
- Perl
- TUWF
- pandoc (>= 3.6)
