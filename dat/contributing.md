% Want to contribute?

Every free/open source software project is run in a different way. To set the
right expectations and prevent disappointment and/or wasted effort, here's a
few notes on how I deal with contributions.

**Feedback** of any kind is always welcome, but I make no promise that I will
act on it. Feel free to open an issue on [my git
forge](https://code.blicky.net/yorhel) or mail me at
[projects@yorhel.nl](mailto:projects@yorhel.nl).

I love **bug reports**, so don't hesitate to report anything that doesn't work
or doesn't work as you had expected.  Whether I will actually fix it depends on
the nature of the bug, of course, but I do very much value stable and reliable
software.

**Feature requests** are welcome, but I tend to not act on most of them and
often reject features that seem out of scope. It's still good to have a list of
potential features to work on if I feel like making myself useful, so new ideas
are nonetheless appreciated.

Feel free to send **patches** and **pull requests** for **trivial fixes** for
simple bugs, documentation issues, typos, etc. If the change is trivial to
review and improves the software in a noticeable way, I'm happy to apply it.
Don't bother submitting patches for things that don't noticeably improve the
software. I don't particularly care about compiler warnings when they don't
affect the correctness of the code, and the churn of patching silly things is
more likely to introduce new bugs instead. Slight inconsistencies in code style
or typos in variable names or comments (i.e. non-user visible parts) don't
really need fixing, either.

When it comes to patches or pull requests for **larger fixes** or **new
features**, I strongly advise you to get in touch first to discuss your planned
changes.  After all, there's a good chance I already have opinions about it.
Generally speaking, I have no intention to merge code that I haven't thoroughly
reviewed, and I enjoy programming much more than I enjoy reviewing other
people's code, so just throwing patches over the fence and expecting me to
merge anything is a recipe for disappointment.

If you use my software in combination with **proprietary software** or a
proprietary OS (like MacOS or Windows), then you're mostly on your own. I write
free software because I care about user freedom, so I'm not inclined to spend
time and effort improving my code to better work with software that I wouldn't
recommend anyone to use. I may accept the occasional fix if it's simple enough,
but I've no intention to bend over backwards.

I do not accept **donations** at the moment, since it's not worth the tax
admininstration hassle. Consider donating to [Bits of
Freedom](https://www.bitsoffreedom.nl/english/) instead.
<small>(alright, I do have a patreon and subscribestar over at vndb, if you <i>really</i> insist)</small>
