% Writing

I don't often write stuff. Certainly not enough to warrant a blog. But
sometimes I do feel the need to write down my thoughts. The results of those
rare occasions are published on this page.

## Articles That May As Well Be Considered Blog Posts

`2022-12-23` - [Overengineering Title Preferences for VNDB](/doc/vndbtitles)
:   A detailed look into the implementation of title preferences on VNDB and a
    bunch of PostgreSQL query optimization experiments.

`2021-07-22` - [Ncdu 2: Less hungry and more Ziggy](/doc/ncdu2)

`2019-08-13` - [From SQL to Nested Data Structures](/doc/sqlobject)
:   How to easily fetch complex nested data structures from a normalized
    relational database.

`2019-05-14` - [Fast Key Lookup with a Small Read-Only Database](/doc/pwlookup)
:   How to quickly check if a password is in a large (but nicely compressed)
    dictionary.

`2017-05-28` - [An Opinionated Survey of Functional Web Development](/doc/funcweb)
:   The title says it all.

`2014-07-29` - [The Sorry State of Convenient IPC](/doc/easyipc)
:   A long rant about IPC systems.

`2014-01-09` - [Some Measurements on Direct Connect File Lists](/doc/dcstats)
:   A short measurement study on the file lists obtained from a Direct Connect
    hub.  Lots of graphs!

`2012-02-15` - [A Distributed Communication System for Modular Applications](/doc/commvis)
:   In this article I explain a vision of mine, and the results of a small
    research project aimed at realizing that vision.

`2011-11-26` - [Multi-threaded Access to an SQLite3 Database](/doc/sqlaccess)
:   So you have a single database and some threads. How do you combine these in
    a program?

## Longer Reports

`2014-06-10` - [Biased Random Periodic Switching in Direct Connect](/download/doc/brpsdc.pdf) (PDF)
:   My masters thesis.

`2013-04-05` - [Peer Selection in Direct Connect](/download/doc/psdc.pdf) (PDF)
:   The rather long-ish literature study that precluded my masters thesis.

`2010-06-02` - [Design and implementation of a compressed linked list library](https://dev.yorhel.nl/download/doc/compll.pdf) (PDF)
:   The report for the final project of my professional (HBO) bachelor of
    Electrical Engineering. I was very liberal with some terminology in this
    report. For example, "linked lists" aren't what you think they are, and I
    didn't even use the term "locality of reference" where I really should
    have. It was also written for an audience with little knowledge on the
    subject, so I elaborated on a lot of things that should be obvious for most
    people in the field. Then there is a lot of uninteresting overhead about
    the project itself, which just happened to be mandatory for this report.
    Nonetheless, if you can ignore these faults it's not such a bad read, if I
    may say so myself. :-)
