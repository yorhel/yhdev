% Code dump

I write a lot of miscellaneous little perl/shell scripts and micro-libraries
for the purpose of getting something done.  This page is a listing of those I
thought might be of useful to others as well.

I also maintain a collection of miscellaneous C micro-libraries. Those are
listed under the collective name of [Ylib](/ylib).

# Subpages

- [Demoscene thingy](/dump/demo)
- [AWStats Data File Schrinker](/dump/awshrink)
- [GTK+ Mass File Renamer](/dump/grenamr)
- [Colours in NCurses](/dump/nccolour)
- [Insertion Performance Benchmarks](/dump/insbench)
- [btrfs-size.pl](/dump/btrfssize)

# Miscellaneous

## maildir.pl

October 2012. A tiny weechat plugin to display the number of unread emails in a
local Maildir. [Latest
source](https://weechat.org/scripts/source/stable/maildir.pl.html/)
([1.0](https://p.blicky.net/wzbzs)).

## ncdc-share-report

December 2011. Playing around with the Go programming language, I wrote another
transfer log parser and statistics generator for ncdc.
[Example output](http://s.blicky.net/2012/ncdc-share-report.html).

Download: [0.3](https://p.blicky.net/h25z8)
([0.2](https://p.blicky.net/6yx2d), [0.1](https://p.blicky.net/ab4lm)).

## ncdc-transfer-stats

September 2011. [ncdc](/ncdc) gained transfer logging features, and I wrote a
quick Perl script to fetch some simple statistics from it.
[source](https://p.blicky.net/4V9Kg59kUJUN)
([0.2](https://p.blicky.net/eu00a), [0.1](https://p.blicky.net/agolr)).

## json.mll

December 2010. I was writing a client for the [public VNDB
API](https://vndb.org/d11) in OCaml and needed a JSON parser/generator. Since I
wasn't happy with the currently available solutions - they try to do too many
things and have too many dependencies - I decided to write a minimal JSON
library myself.  [source](https://g.blicky.net/serika.git/tree/json.mll)

## vinfo.c

November 2009. The [public VNDB API](https://vndb.org/d11) was designed to be
easy to use even from low level languages. I wrote this simple program to see
how much work it would be to use the API in C, and as example code for anyone
wishing to use the API for something more useful. Read the comments for more
info.  [source](/download/code/vinfo.c)

## Microdc2 log file parser

June 2007. Simple perl script that parses log files created by
[microdc2](http://corsair626.no-ip.org/microdc/) and outputs a simple and ugly
html file with all uploaded files. It correctly merges chunked uploads,
calculates average upload speed per file and total bandwidth used for uploads.
[source](/download/code/mdc2-parse.pl)

**Note:** for those of you who still use microdc2, please have a look at
[ncdc](/ncdc), a less ancient alternative.

## yapong.c

Feburary 2006. Yet Another Pong, and yet another program written just for
testing/ learning purposes. Tested to work with the ncurses or pdcurses
libraries.  [source](/download/code/yapong.c) ([older
version](/download/code/yapong-0.01.c)).

## echoserv.c

February 2006. A simple non-blocking single-threaded TCP echo server,
displaying how the select() system call can be used to handle multiple
connections.  [source](/download/code/echoserv.c)

## bbcode.c

January 2006. Simple BBCode to HTML converter written in plain C, for learning
puroses.  [source](/download/code/bbcode.c)
