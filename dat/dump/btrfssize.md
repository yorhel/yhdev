% btrfs-size.pl

_2016-08-16_ - btrfs-size.pl is a quick little script to provide an overview of
the disk space used by btrfs subvolumes. It's comparable to
[btrfs-size.sh](https://poisonpacket.wordpress.com/2015/05/26/btrfs-snapshot-size-disk-usage/),
but is somewhat faster and has a few options to sort the output.

Honestly, it's still hard to draw any conclusions from the sizes provided by
btrfs, but sadly, [ncdu](/ncdu) is useless for snapshot-heavy filesystems.

Only tested with btrfs-progs v4.4.1.

**Download:** [btrfs-size.pl](https://p.blicky.net/FNPXpbwMXfTI.txt)
([syntax-highligted version](https://p.blicky.net/FNPXpbwMXfTI)).

## Usage

    btrfs-size.pl --help [-nser] <path>
      -n  Order by path name
      -s  Order by (total) subvolume size
      -e  Order by exclusive subvolume size
      -r  Reverse order

## Example output

    # btrfs-size.pl /data
    gfbf007/cur                               46.32 GiB   16.00 KiB
    gfbf007/snap/2016-08-14.08                46.32 GiB  428.00 KiB
    gfbf007/snap/2016-08-15.03                46.32 GiB  428.00 KiB
    gfbf007/snap/2016-08-16.03                46.32 GiB   16.00 KiB
    ggit011/cur                               23.92 MiB   16.00 KiB
    ggit011/snap/2016-08-14.08                23.90 MiB  300.00 KiB
    ggit011/snap/2016-08-15.08                23.92 MiB   16.00 KiB
    gman015/cur                                3.74 GiB   16.00 KiB
    gman015/snap/2016-08-14.08                 3.74 GiB  112.00 KiB
    gman015/snap/2016-08-15.02                 3.74 GiB   96.00 KiB
    gman015/snap/2016-08-16.02                 3.74 GiB   16.00 KiB
