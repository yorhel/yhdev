% Demos

Yes, I realise that the title is plural, suggesting there's more than one demo.
That is not quite true, unfortunately. The reason I chose to use plural form is
simply in the hopes that I do, in fact, write more demos, and that this page
will actually get more content in the future. I still happen to be a huge fan
of the [demoscene](http://demoscene.info/), and still wish to contribute to
it... if only I could find the time and self-discipline to do so. In the
meanwhile, here's one demo I did write some time ago.

*(2019 update: Don't get your hopes up, I likely won't ever write another demo.
I don't have the patience for it, I guess.)*

# Blue Cubes

![Blue Cubes.](/img/bluecubes.png){.right}
August 2006. My first demo - or more exact: intro. Blue Cubes is a 64kB intro
written in OpenGL/SDL with Linux as target OS. I wrote this intro within 10
days without any prior experience in any of the fields of computer generated
graphics or music. So needlessly to say, it sucks. I am ashamed even of the
thought of releasing it at a respectable demoparty like
[Evoke](https://www.evoke.eu/2006/). Still, it didn't feel I was unwelcome, I
did actually receive three prices: 3rd price in the 64k competition (there were
only 3 actual entries, but oh well), best non-windows 64k intro (it was the
only one in the competition), and the Digitale Kultur newcomer award, which
actually is something to be proud of, I guess.

[download](/download/yorhel~bluecubes.zip) -
[mirror](https://scene.org/file.php?file=/parties/2006/evoke06/in64/yorhel_bluecubes.zip&fileinfo)
(includes linux binaries, windows port, and sources) -
[pouet comments](https://pouet.net/prod.php?which=25866).
