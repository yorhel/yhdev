% GTK+ Mass File Renamer

GRenamR is a GTK+ mass file renamer written in Perl, the functionality is
insipred by the
[rename](https://search.cpan.org/~rmbarker/File-Rename-0.05/rename.PL) command
that comes with a Perl module.

GRenamR allows multiple file renaming using perl expressions. You can see the
effects of your expression while typing it, and can preview your action before
applying them.  The accepted expressions are mostly the same as the rename
command (see above paragrah): your expression will be evaluated with `$_` set
to the filename, and any modifications to this variable will result in the
renaming of the file. There's one other variable that the rename command does
not have: `$i`, which reflects the file number (starting from 0) in the current
list. This allows expressions such as as `$_=sprintf'%03d.txt',$i`.

**Download:** [grenamr](/download/code/grenamr-0.1.pl)
(copy to /usr/bin/ to install)

Requires the Gtk2 Perl module. Most distributions have a perl-gtk2 package.

## Example expressions

    y/A-Z/a-z/                       # Convert filenames to lowercase
    $_=lc                            # Same
    s/\.txt$/.utf8/                  # Change all '.txt' extensions to '.utf8'
    s/([0-9]+)/sprintf'%04d',$1/eg   # Zero-pad all numbers in filenames

    # Replace each image filename with a zero-padded number starting from 1
    s/^.+\.jpg$/sprintf'%03d.jpg',$i+1/e

## Caveats / bugs / TODO

- Calling functions as 'sleep' or 'exit' in the expression will trash the program
- It's currently not possible to manually order the file list, so $i is
not useful in every situation
- It's currently not possible to manually rename files or exclude items
from being effected by the expression
- The expression isn't executed in the opened directory, so things like
[-X](https://perldoc.perl.org/functions/-X.html) won't work

## Screenshot

![GRenamR screenshot](/img/grenamr.png){.scr}
