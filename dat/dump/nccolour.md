% Colours in NCurses

I decided to do some experimentation with how the colours defined in ncurses
are actually displayed in terminals, what the effects are of combining these
colours with other attributes, and how colour schemes of a terminal can affect
the displayed colours. To this end I wrote a small c file and ran it in
different terminals and different configurations. Note that only the 8 basic
NCurses colours are tested, the more flexible init\_color() function is not
used.

**Source code:** [nccolour.c](/download/code/nccolour.c)
([syntax highlighed version](http://p.blicky.net/xu35c))

## Notes / observations

- The most obvious conclusion: the displayed colours do not have the exact same
  colour value in every terminal. Some terminals also allow users to modify
  these colours.
- You can not assume that the default foreground or background colour can be
  represented by one of the 8 basic colours defined by NCurses.
- Specifying -1 as colour, to indicate the default foreground or background
  colour, seems to work fine in any terminal tested so far.
- All tested terminals render the foreground colour in a lighter shade when the
  A\_BOLD attribute is set. This does not apply to the background colour.  The
  result of this is that the text becomes visible when using A\_BOLD when the
  foreground and background colour are set to the same value.
- Unfortunately, not all terminals are configured in such a way that all
  possible colours are readable. So as a developer you'll still have to support
  configurable colour schemes in your ncurses application. :-(
- On most terminals, setting the foreground and background colour to the same
  value without applying the A\_BOLD attribute will make the text invisible.
  Don't rely on this, however, as this is not the case on OS X.

## Full screenshot

To avoid wasting unecessary space, the comparison screenshots below only
display the colour table. Here's a screenshot of the full output of the
program, which also explains what each column means.

![Full screenshot](/img/nccol-full.png)

## Screenshots

Arch Linux, Roxterm, Default color scheme

![](/img/nccol-rox-b.png)

Arch Linux, Roxterm, GTK color scheme

![](/img/nccol-rox-w.png)

Arch Linux, Roxterm, Tango color scheme

![](/img/nccol-rox-t.png)

Arch Linux, Roxterm, Modified Tango color scheme

![](/img/nccol-rox-c.png)

Arch Linux, xterm (default settings)

![](/img/nccol-xterm.png)

Ubuntu 11.10, Gnome-terminal

![](/img/nccol-ubuntu.png)

Debian Squeeze, VT (default settings)

![](/img/nccol-debian.png)

FreeBSD, VT (default settings)

![](/img/nccol-fbsd.png)

Mac OS X, Terminal

![](/img/nccol-osx-terminal.png)

Mac OS X, iTerm2

![](/img/nccol-osx-iterm2.png)

CentOS 6.4

![](/img/nccol-centos64.png)
