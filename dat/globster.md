% The Globster Direct Connect Client

<b style="color: #f00">Project Abandoned</b><br>
I've stopped development on Globster. I still believe the overall idea and
architecture of Globster are good, and the DC community would definitely
benefit from a remotely controllable client, but Globster in its current form
wasn't going into the direction I wanted it to. I might restart the project
from scratch (yet again) in the future, but for now... it's as dead as a cute
zombie whale.

<div style="width: 600px; height: 227px; background-image: url(/img/globster.png); margin-bottom: -30px">
<b style="font-size: 14px; position: relative; left: 150px; top: 10px">The Globster What?</b>
<p style="position: relative; left: 150px; top: 20px; width: 420px; text-align: right">
Globster is an efficient file sharing client for the Direct Connect<br>
network. It runs as a background daemon and provides<br>
a convenient and high-level D-Bus API, making<br>
it easy to write scripts, bots and user<br>
interfaces for Direct Connect.
</p></div>

# Adopt your own Globster

## Download

There are no tarballs at the moment. You'll have to get it from the git repo:

    git clone --recursive git://g.blicky.net/globster.git
    cd globster
    autoreconf -i
    ./configure
    make
    sudo make install

When doing a `git pull` to update your version later on, make sure to follow
up with a `git submodule update` to get the right dependencies, too.

The git repo is available for
[online browsing](https://g.blicky.net/globster.git/).

## Requirements

Globster can be compiled with a (moderately recent) GCC or clang. You'll need
the following libraries: [libdbus](http://dbus.freedesktop.org/),
[GnuTLS](http://gnutls.org) and [zlib](http://zlib.net/). If your GnuTLS is too
old (<= 2.12), you also need libgcrypt. The globsterctl script requires Perl
and the Net::DBus module.

On Debian and Ubuntu, that boils down to the following:

    apt-get install git make gcc libc-dev automake autoconf\
      pkg-config libdbus-1-dev libgnutls-dev libnet-dbus-perl

And for Arch Linux:

    pacman -S base-devel git perl-net-dbus

I've only tested things on Linux (glibc and [musl](http://www.musl-libc.org)),
but I intent to support more kinda-sane POSIX systems in the future as well.
Globster will no doubt require some more libraries as more basic features are
being implemented. And, yes, _of course_ we will get static binaries!

## Status

Remember when I called Globster a "file sharing" client? I lied. It doesn't
share or download files yet, since it's currently in an early alpha stage. So
what _does_ it do?

- Connect to ADC and NMDC hubs
- User list management
- Chatting and private messaging

Those features already make it perfectly suitable for writing chat-only bots
and interfaces.

## Usage

Globster isn't particularly hard to use, but usage documentation is currently a
bit lacking. I have every intention to fix that, but for now, you're encouraged
to join the development hub and bug me for help: `adc://dc.blicky.net:2780/`.
I did already write some [API documentation](/globster/api).

There are at this point not many scripts or interfaces available for Globster:

- [globsterctl](/globster/ctl) - A control script for the daemon, included in the git repo.
- [globster-feedspam.pl](http://p.blicky.net/0z9uw) - An RSS / Atom notification script.
- [globster-mhc.pl](http://p.blicky.net/8y8mv) - A hub chat link script. More useful as an example than anything else.
- [globgraph](http://p.blicky.net/qvg59) - Munin plugin to monitor Direct Connect hubs.

There's more to come. I'd love to have at least a convenient console client (a
weechat or irssi plugin? An ncdc fork?) and perhaps a web-based interface. But
other stuff is welcome, too. Who's going to write all that, you ask? Erm...
well... You, perhaps? :-)

## Final notes

As you've come to expect from me _(right?)_, Globster is entirely written in C
and available under a liberal MIT license.

Globster incorporates code from
[libev](http://software.schmorp.de/pkg/libev.html),
[freetiger](http://klondike.es/freetiger/),
[klib](https://github.com/attractivechaos/klib) and
[ylib](https://dev.yorhel.nl/ylib).
Additionally, [autoconf-lean](https://bitbucket.org/GregorR/autoconf-lean) is
used to keep the configure script fast.
