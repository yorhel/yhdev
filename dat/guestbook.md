% Guestbook

It was 2022 and I figured this little site could use a guestbook. Want to leave
a message? Fill out the form below. Messages are added manually, so it may take
a day or two for yours to show up. Markdown is supported.

This guestbook exists just for sillyness; if you have a question or want to
report feedback for a particular project, better open an issue at the
respective issue tracker or send a mail to
[projects@yorhel.nl](mailto:projects@yorhel.nl) instead.

<form method="POST" action="/cgi-bin/guestbook.cgi">
<input type="text" name="name" placeholder="Your name (optional)">
<textarea name="message" placeholder="Message" required cols=50 rows=5></textarea>
<input type="submit" value="Submit">
</form>

# Entries

`2025-02-16` - ~
:   ncdu is a great app. Keep up the great efforts! Respect from Canada! - Adeel Khan at Awake and Shangrila-VHP

`2025-01-12` - alkatraz445
:   Absolutely lovely tool. Did the thing fast and quit even quicker. Instant recommendation

`2025-01-01` - ~
:   Happy New Year!  Thanks for NCDU!

`2025-01-01` - CirqueForge
:   Happy New Year and I hope you have a great 2025 and beyond. On Christmas
    Day 2024, I released source code for software tools publicly that has
    quietly used yxml for quite a number of years privately. Thank you for the
    development and effort you put into the project.

`2024-12-29` - tetz
:   ncdu is great! Thank you!

`2024-11-31` - NocFA
:   Thanks for your incredible software, NCDU has been in my sysadmin toolkit for years.
    Appreciate incredible FOSS developers like you making the world a better place, one piece of quality software at a time.

    If I find a donate button somewhere in your pages, I shall be abusively clicking on it.

`2024-10-10` - Hamster
:   Hey there, use the App since years. Really enjoy it, many thanks!

`2024-08-17` - Young Lee
:   hey, thanks for the nice app!

`2024-08-08` - raksO
:   Thanks! It is really helpful! ❤️

`2024-07-12` - DIANA PUNKY
:   ncdu rocks!

`2024-05-29` - andy
:   ncdu!

`2024-04-19` - tibs
:   Thanks for all of your hard work. Your software has enhanced my life
    throughout the years!

`2024-04-13` - sku1d
:   Because `ncdu` does everything amazingly right I was wondering who is the
    author of it. That is how I found this page and along with it a demo scene
    fellow. What a nice surprise! Thanks for making the world a better place.
    Trying the same here.

`2024-02-26` - Vinay
:   This tool is my go to to clear up space. So simple and works perfectly. I
    have shared it with lot of people, and will keep spreading it. Thanks for
    providing this to the world. :)

`2024-01-26` - Walt
:   Not for the first time - but thanks. I've used NCDC for years. It's the
    only DC server/client worth having on a Raspberry Pi. Before NCDC, I had an
    entire old desktop machine dedicated to this and other upload/download
    duties.  With NCDC and a couple of other NAS things running, I do it all on
    15 watts.

`2024-01-21` - cancername
:   nice software you have here, I admire their simplicity

`2024-01-19` - WhatTheF
:   Holy shit, I just lost over 200GB of valuable data. Maybe I just pressed D
    instead of CTRL+C, but I don't know how I got past that confirmation dialog
    that I didn't even see. I don't know of any other tool where D is for
    deletion. If only those files were in the recycle bin, but they are not.

`2024-01-12` - J
:   Thanks for NCDU - it's a simple, really useful little utility \<3

`2023-11-20` - Zael
:   Thank you very much for the NCdu! I have been happily using it for years,
    and somehow just now made it to your homepage - where I am sure I shall
    discover other lovely utils. ^^

`2023-10-27` - kspes
:   I love this tool so much! I use it on all my unix and mac machines quite often :)

`2023-09-11` - NeedM
:   Nice client program but only 32bit version?

    *Yorhel comment:* Uhm, where did you get that idea? All software is available in 64bit as well.

`2023-08-28` - Alex
:   I can't believe I have been a sysadmin for so long and have only just found out about this project. It has made my day.

    Thanks for the effort.

`2023-06-19` - ~
:   Discovered your solution through this @AdrienLinuxtricks video => [https://youtu.be/w7KsFQJyXlc](https://youtu.be/w7KsFQJyXlc)

    Thank you for your work!

`2023-05-11` - ~
:   awesome !!

`2023-02-22` - ~
:   thanks!!!

`2023-02-18` - ~
:   thanks for this. it rocks

`2023-02-09` - D.S.
:   Thank you for this Program, for me the best, easy and fast Thank's

`2023-01-19` - Jay
:   ncdu is my go-to tool to figure out why a server is filling up. It's super fast, simple, and effective, and exactly what you want to see in a TUI application.

    My hard drives thank you!

`2022-12-02` - rob
:   Holly Molly! ncdu is life changing!

    Especially when Docker is filling up /var/lib and you have no idea, because all the GUI disk usage apps are hiding it.

`2022-11-13` - S.
:   I friggin' love ncdu. Thanks for bringing it to life! :)

    Greetings from Montevideo, Uruguay.

`2022-10-12` - Anton P
:   Even silly has a meaning.

    I love to see craftsmanship, thanks!

`2022-08-18` - Andrey
:   Zig well, such a choice. Compiling it on gentoo also requires 10GB of ram, which I don't have.

`2022-06-18` - Twelve Kanaw
:   Thank you for your work on NCDC. It has no competition for running on Raspberry Pi. I've used it for years now and never found it lacking in any way. Work like yours is the life blood of free range information space.

`2022-05-30` - ~
:   what a lovely, silly idea to bring back life to the internet guestbook! and thanks for writing and maintaining ncdc!

`2022-05-06` - ~
:   > ncdu-1.17: Add ‘dark-bg’ color scheme and use that by default

    Never expected to see this backported from ncdu-2, totally disaster to me. What's wrong with monochrome and my lovely terminal background image? :((

`2022-04-17` - ~
:   I ncdu!

`2022-04-13` - Thomas B
:   first ;)
