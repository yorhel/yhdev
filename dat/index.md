% Yorhel's Projects

Hello visitor!

Welcome to my little website. I spend a fair amount of my free time developing
and contributing to free and open source software.  This website is home to
several of these projects. I hope that you'll find something useful among the
crap I've written over the years. :)

<!-- These announcements are parsed by mkfeed.pl, see that file for formatting -->
## Announcements <a href="/feed.atom"><img src="/img/feed_icon.png" alt="Atom feed"></a>

`2025-03-05` - ncdu 1.22 & 2.8 released <!-- tags: ncdu, link: /ncdu -->
:   These add support for ignoring errors in the config file and now list all
    supported options in `--help`. 1.22 backports `--graph-style` and 2.8 now
    requires Zig 0.14. [Homepage](/ncdu) - [v1 Changelog](/ncdu/changes) -
    [v2 Changelog](/ncdu/changes2).

`2025-03-04` - ncdc 1.25 released <!-- tags: ncdc, link: /ncdc -->
:   Adds support for IP to country lookups through
    [libloc](https://www.ipfire.org/location/), as a more free alternative to
    the MaxMind country database.  [Homepage](/ncdc) -
    [Changelog](/ncdc/changes).

`2024-11-19` - ncdu 2.7 released <!-- tags: ncdu, link: /ncdu -->
:   Adds support for transparent compression for JSON import and export, tilde
    expansion in the config file, new `--export-block-size` and `--compress`
    flags and a few minor bug fixes. [Homepage](/ncdu) -
    [Changelog](/ncdu/changes2).

`2024-11-19` - ncdu 1.21 released <!-- tags: ncdu, link: /ncdu -->
:   Adds support for tilde expansion in the config file and backports a few
    fixes from the 2.x branch. [Homepage](/ncdu) - [Changelog](/ncdu/changes).

`2024-10-22` - nginx-confgen 2.2 released <!-- tags: nginx-confgen, link: /nginx-confgen -->
:   Adds support for `$#array` syntax and fixes a minor bug.
    [Homepage](/nginx-confgen) - [Changelog](/nginx-confgen/changes).

`2024-09-27` - ncdu 2.6 released <!-- tags: ncdu, link: /ncdu -->
:   Adds a new binary export format that better works with parallel scanning,
    offers built-in compression and supports browsing directory trees that are
    too large to fit in memory. [Homepage](/ncdu) - [Changelog](/ncdu/changes2).

`2024-07-24` - ncdu 2.5 released <!-- tags: ncdu, link: /ncdu -->
:   Adds support for parallel scanning, improves import/export performance and
    fixes a number of bugs. [Homepage](/ncdu) - [Changelog](/ncdu/changes2).

`2024-07-18` - ncdc 1.24.1 released <!-- tags: ncdc, link: /ncdc -->
:   Just fixes a build error. [Homepage](/ncdc) - [Changelog](/ncdc/changes).

`2024-04-21` - torreas 0.3 released <!-- tags: torreas, link: /torreas -->
:   Updated for Zig 0.12. [Homepage](/torreas) - [Changelog](/torreas/changes).

`2024-04-21` - ncdu 1.20 & 2.4 released <!-- tags: ncdu, link: /ncdu -->
:   Man page rewritten in *mdoc*, revert the default color scheme to *off*, Zig
    0.12 and some minor stuff.  [Ncdu homepage](/ncdu) -
    [Changelog](/ncdu/changes).

`2024-01-07` - ncdc 1.24 released <!-- tags: ncdc, link: /ncdc -->
:   A bunch of small fixes and updates that accumulated over the years, nothing
    special. [Ncdc homepage](/ncdc) - [changelog](/ncdc/changes)

`2023-12-10` - torreas 0.2 released <!-- tags: torreas, link: /torreas -->
:   Adds support for partial reassembly and fixes a bug.
    [Homepage](/torreas) - [Changelog](/torreas/changes).

`2023-11-03` - New tool: torreas <!-- tags: torreas, link: /torreas -->
:   I wrote a little tool to reassemble torrent files, more information
    available on its [homepage](/torreas).

`2023-09-11` - ncdu 1.19 released <!-- tags: ncdu, link: /ncdu -->
:   Fixes a typo in the `--exclude-from` option and backports the CLI option to
    disable natural sorting and the little indicator in the footer whether
    apparent size or disk usage is being displayed. [Ncdu homepage](/ncdu) -
    [Changelog](/ncdu/changes).

`2023-08-04` - ncdu 2.3 released <!-- tags: ncdu, link: /ncdu -->
:   This release adds a CLI option to disable natural sorting, a little
    indicator in the footer whether apparent size or disk usage is being
    displayed and a few build system improvements. This release requires Zig
    0.11. [Ncdu homepage](/ncdu) - [Changelog](/ncdu/changes2).

`2023-02-12` - ncdu 1.18.1 released <!-- tags: ncdu, link: /ncdu -->
:   Fix bug causing builds to fail on platforms other than Linux.
    [Ncdu homepage](/ncdu) - [Changelog](/ncdu/changes).

`2023-01-19` - ncdu 2.2.2 released <!-- tags: ncdu, link: /ncdu -->
:   Updated for Zig 0.10.x, compilation with 0.9.x is no longer supported.
    [Ncdu homepage](/ncdu) - [Changelog](/ncdu/changes2).

`2023-01-18` - TUWF 1.5 released <!-- tags: tuwf, link: /tuwf -->
:   This release contains several years worth of fixes and feature additions.
    [Homepage](/tuwf) - [Changelog](/tuwf/changes)

`2022-12-23` - New article: Overengineering Title Preferences for VNDB <!-- link: /doc/vndbtitles -->
:   A detailed look into the implementation of title preferences on VNDB and a
    bunch of PostgreSQL experiments. [Read more.](/doc/vndbtitles)

`2022-12-06` - ncdu 1.18 released <!-- tags: ncdu, link: /ncdu -->
:   Backports configuration file support and a whole bunch of CLI flags from
    the 2.x branch. Also fixes drawing a pwoper background with the default
    color scheme. [Ncdu homepage](/ncdu) - [Changelog](/ncdu/changes).

`2022-10-25` - ncdu 2.2.1 released <!-- tags: ncdu, link: /ncdu -->
:   Fixes a bug with color rendering on FreeBSD and MacOS.
    [Ncdu homepage](/ncdu) - [Changelog](/ncdu/changes2).

`2022-10-17` - ncdu 2.2 released <!-- tags: ncdu, link: /ncdu -->
:   Improves exclude pattern matching performance, draws a proper background
    with the default color scheme and fixes a bug with exporting file names
    containing certain control characters.  [Ncdu homepage](/ncdu) -
    [Changelog](/ncdu/changes2).

`2022-09-27` - ncdc 1.23.1 released <!-- tags: ncdc, link: /ncdc -->
:   Fixes a buffer overflow in IPv6 address formatting.
    [Ncdc homepage](/ncdc) - [changelog](/ncdc/changes)

`2022-05-30` - ncdc 1.23 released <!-- tags: ncdc, link: /ncdc -->
:   A bunch of small fixes and updates that accumulated over the years, nothing
    special. [Ncdc homepage](/ncdc) - [changelog](/ncdc/changes)

`2022-04-28` - ncdu 1.17 and 2.1.2 released <!-- tags: ncdu, link: /ncdu -->
:   2.1.2 fixes a possible crash when shortening unicode filenames and 1.17
    improves system compatibility and backports a few features from the 2.x
    branch: colors by default and natural sorting. [Ncdu homepage](/ncdu) -
    [v1 changelog](/ncdu/changes) - [v2 changelog](/ncdu/changes2).

`2022-04-27` - nginx-confgen 2.1 released <!-- tags: nginx-confgen, link: /nginx-confgen -->
:   Improved handling of map blocks and a few minor fixes.
    [Homepage](/nginx-confgen) - [Changelog](/nginx-confgen/changes).

`2022-03-25` - ncdu 2.1.1 released <!-- tags: ncdu, link: /ncdu -->
:   A minor bugfix release: fixes a typo, a possible crash when refreshing and
    reverts the default `--graph-style` to hash characters.
    [Ncdu homepage](/ncdu) - [Changelog](/ncdu/changes2).

`2022-02-07` - ncdu 2.1 released <!-- tags: ncdu, link: /ncdu -->
:   This release order file names using natural sorting, unicode box drawing
    characters for the size bar, a `--graph-style` option, and fixes a few
    minor bugs. [Ncdu homepage](/ncdu) - [Changelog](/ncdu/changes2).

`2022-01-01` - ncdu 2.0.1 released <!-- tags: ncdu, link: /ncdu -->
:   Just two minor build fixes: one fix for systems where the `wcwith()`
    function wasn't correctly imported, another fix to add a `ZIG_FLAGS` option
    to the Makefile. [Ncdu homepage](/ncdu) - [Changelog](/ncdu/changes2).

`2021-12-21` - ncdu 2.0 released <!-- tags: ncdu, link: /ncdu -->
:   The first stable release of the new ncdu rewrite in Zig. This comes with
    several UI and performance improvements over the C version. Requires Zig
    0.9.0. [Ncdu homepage](/ncdu) - [Changelog](/ncdu/changes2).

`2021-07-22` - ncdu 2.0-beta1 released <!-- tags: ncdu, link: /doc/ncdu2 -->
:   This marks the initial beta version of a complete rewrite of ncdu, written
    in Zig. This version significantly reduces memory usage and improves hard
    link counting. [Full release announcement](/doc/ncdu2) - [Ncdu homepage](/ncdu).

`2021-07-02` - ncdu 1.16 released <!-- tags: ncdu, link: /ncdu -->
:   A minor feature & bugfix release. This adds dynamic sizing of the file size
    bar, an `$NCDU_LEVEL` environment variable when spawning a subshell, more
    file mode flags and allows for a few future extensions to the JSON dump
    format. [Ncdu homepage](/ncdu) - [Changelog](/ncdu/changes).

`2020-10-22` - nginx-confgen 2.0 released <!-- tags: nginx-confgen, link: /nginx-confgen -->
:   Completely rewritten in C for easier, more portable and more lightweight
    builds. Variables are now globally scoped, `pre_if` supports braces,
    improved error reporting and a whole bunch of other improvements.
    [Homepage](/nginx-confgen) - [Changelog](/nginx-confgen/changes).

`2020-06-10` - ncdu 1.15.1 released <!-- tags: ncdu, link: /ncdu -->
:   Fixes two bugs introduced in 1.15: Fix building on older Linux systems and
    revert to the old follow-firmlinks-by-default behavior on macOS.
    [Ncdu homepage](/ncdu) - [Changelog](/ncdu/changes).

`2020-05-30` - ncdu 1.15 released <!-- tags: ncdu, link: /ncdu -->
:   This release adds support for excluding Linux pseudo filesystems
    (`--exclude-kernfs`), detects and skips MacOS firmlinks by default (old
    behavior is available through `--follow-firmlinks`) and fixes a few bugs.
    [Ncdu homepage](/ncdu) - [Changelog](/ncdu/changes).

`2020-02-10` - ncdu 1.14.2 released <!-- tags: ncdu, link: /ncdu -->
:   Another minor bugfix release. This fixed compilation on GCC 10, reduces
    memory usage for hardlink detection a little bit and fixes a minor display
    issue when scanning more than 10 million files. [Ncdu homepage](/ncdu) -
    [Changelog](/ncdu/changes).

`2019-08-13` - New article: From SQL to Nested Data Structures <!-- link: /doc/sqlobject -->
:   How to easily fetch complex nested data structures from a normalized
    relational database.  [Read more.](/doc/sqlobject)

`2019-08-05` - ncdu 1.14.1 released <!-- tags: ncdu, link: /ncdu -->
:   A minor bugfix release. This fixes the `--exclude-caches` option,
    intermittend early exit on OS X and improves handling of out-of-memory
    scenarios. [Ncdu homepage](/ncdu) - [Changelog](/ncdu/changes).

`2019-07-06` - TUWF 1.3.1 & 1.4 released <!-- tags: tuwf, link: /tuwf -->
:   Just fixes a minor issue with the tests on older Perls. [Homepage](/tuwf) -
    [Changelog](/tuwf/changes)

`2019-06-17` - TUWF 1.3 released <!-- tags: tuwf, link: /tuwf -->
:   This release introduces a new and much more flexible data validation module
    to replace the old `kv_validate()`. Validation schemas defined this way can
    also be used for JSON coercion, HTML5 validation pattern generation and
    code generation for Elm - but those features are all still somewhat
    experimental and undocumented. Additionally there's now an option to have
    outgoing emails written to the log file instead of actually sending them,
    which is useful when debugging.  [Homepage](/tuwf) -
    [Changelog](/tuwf/changes)

`2019-06-03` - ncdc 1.22.1 released <!-- tags: ncdc, link: /ncdc -->
:   This release fixes a segfault that may happen when connecting with other
    ADC clients. This bug was introduced in 1.22. Additionally, static binaries
    are now available for AArch64. [Homepage](/ncdc) -
    [Changelog](/ncdc/changes).

`2019-05-14` - New article: Fast Key Lookup with a Small Read-Only Database <!-- link: /doc/pwlookup -->
:   How to quickly check if a password is in a large (but nicely compressed)
    dictionary. Some code and a few benchmarks. [Read more.](/doc/pwlookup)

`2019-04-30` - ncdc 1.22 released <!-- tags: ncdc, link: /ncdc -->
:   There has been a little bit of renewed interest since the 1.21 release, so
    we have a few (small) new features and improvements. It's now possible to
    browse someone's file list directly from the connections tab, there's
    support for multiple upload slots per user and support for ALPN on TLS
    hubs.  [Homepage](/ncdc) - [Changelog](/ncdc/changes).

`2019-03-26` - ncdc 1.21 released <!-- tags: ncdc, link: /ncdc -->
:   A long-overdue release of ncdc with some improvements that have been
    sitting idle in the git repository for far too long. This release adds
    support for libmaxminddb, free slot broadcasting, skipping downloading
    files you already have and indicating shared/queued files in the search
    window and file browser. [Homepage](/ncdc) - [Changelog](/ncdc/changes).

`2019-03-23` - Bug tracker migration to Gitea <!-- tags: ncdu, ncdc, yxml -->
:   The bug trackers that used to be embedded in this website have now been
    migrated to [Gitea](https://code.blicky.net/) issues. The bug trackers of the
    following projects can now be found at these new locations:

    - Ncdu: [/yorhel/ncdu/issues](https://code.blicky.net/yorhel/ncdu/issues)
    - Ncdc: [/yorhel/ncdc/issues](https://code.blicky.net/yorhel/ncdc/issues)
    - Yxml: [/yorhel/yxml/issues](https://code.blicky.net/yorhel/yxml/issues)

`2019-02-04` - ncdu 1.14 released <!-- tags: ncdu, link: /ncdu -->
:   This release adds mtime display & sorting, a limited form of
    --follow-symlinks, can display larger file counts in the file browser and
    fixes a few bugs you weren't likely to trigger anyway. [Ncdu
    homepage](/ncdu) - [Changelog](/ncdu/changes).

`2018-02-23` - nginx-confgen 1.2 released <!-- tags: nginx-confgen, link: /nginx-confgen -->
:   This release significantly improves the parser to accept more syntax and
    fixes string quoting, variable formatting and escape sequences.
    [Homepage](/nginx-confgen) - [Changelog](/nginx-confgen/changes).

`2018-02-18` - TUWF 1.2 released <!-- tags: tuwf, link: /tuwf -->
:   This release includes a large number of API additions to improve ergonomics
    and adds support for a build-in web server, JSON requests/responses and a
    whole lot more.  [Homepage](/tuwf) - [Changelog](/tuwf/changes).

`2018-01-29` - ncdu 1.13 released <!-- tags: ncdu, link: /ncdu -->
:   This release adds color support and an extended information mode, keeping
    track of file mode, ownership and modification times. Also fixes a few
    bugs. [Ncdu homepage](/ncdu) - [Changelog](/ncdu/changes).

<!-- Old style non-descriptive announcements, included for historical .atom feed stuff.
`2018-01-24` - nginx-confgen 1.1 released <!- tags: nginx-confgen, link: /nginx-confgen ->
`2018-01-19` - New project: nginx-confgen <!- tags: nginx-confgen, link: /nginx-confgen ->
`2017-05-28` - New article: An Opinionated Survey of Functional Web Development <!- link: /doc/funcweb ->
`2016-12-30` - ncdc 1.20 released <!- tags: ncdc, link: /ncdc ->
`2016-08-24` - ncdu 1.12 released <!- tags: ncdu, link: /ncdu ->
`2016-08-16` - Uploaded btrfs-size.pl <!- link: /dump/btrfssize ->
`2015-09-27` - TUWF 1.0 released <!- tags: tuwf, link: /tuwf ->
`2015-04-05` - ncdu 1.11 released <!- tags: ncdu, /ncdu ->
`2014-07-29` - New article: The Sorry State of Convenient IPC <!- link: /doc/easyipc ->
`2014-06-25` - Uploaded my masters thesis <!- link: /doc ->
`2014-04-23` - ncdc 1.19.1 released <!- tags: ncdc, link: /ncdc ->
`2014-02-11` - ncdc 1.19 released <!- tags: ncdc, /ncdc ->
`2014-01-09` - Uploaded an article on DC file list stats <!- link: /doc/dcstats ->
`2013-11-14` - yxml now has a manual <!- tags: yxml, link: /yxml/man ->
`2013-10-05` - ncdc 1.18.1 released <!- tags: ncdc, /ncdc ->
`2013-09-25` - ncdc 1.18 released <!- tags: ncdc, /ncdc ->
`2013-09-03` - Announcing yxml: A small, fast and correct XML parser <!- tags: yxml, link: /yxml ->
`2013-07-05` - Documented a little data structure benchmark <!- link: /dump/insbench ->
`2013-06-15` - ncdc 1.17 released <!- tags: ncdc, link: /ncdc ->
`2013-05-09` - ncdu 1.10 released <!- tags: ncdu, link: /ncdu ->
`2013-04-04` - Created a page for Ylib <!- link: /ylib ->
`2013-04-03` - Created a mailing list for ncdc <!- tags: ncdc, link: /ncdc ->
`2013-03-23` - ncdc 1.16.1 released. <!- tags: ncdc, link: /ncdc ->
`2013-03-02` - ncdc 1.15 released. <!- tags: ncdc, link: /ncdc ->
`2012-12-15` - Announcing yet another awesome project: Globster! <!- tags: globster, link: /globster ->
`2012-12-02` - Documented the ncdu export file format <!- tags: ncdu, link: /ncdu/jsonfmt ->
`2012-11-04` - ncdc 1.14 released <!- tags: ncdc, link: /ncdc ->
`2012-10-17` - Added reference to my repo of small C libs to the code dump <!- link: /dump ->
`2012-10-07` - Added maildir.pl to the code dump <!- link: /dump#maildir.pl ->
`2012-09-27` - ncdu 1.9 released. <!- tags: ncdu, link: /ncdu ->
`2012-09-25` - Added dbusev.c to the code dump <!- link: /dump#dbusev.c ->
`2012-08-16` - ncdc 1.13 released. <!- tags: ncdc, link: /ncdc ->
`2012-07-10` - ncdc 1.12 released. <!- tags: ncdc, link: /ncdc ->
`2012-05-15` - ncdc 1.11 released. <!- tags: ncdc, link: /ncdc ->
`2012-05-03` - Added installation instructions for ncdc. <!- tags: ncdc, link: /ncdc/install ->
`2012-05-03` - ncdc 1.10 released. <!- tags: ncdc, link: /ncdc ->
`2012-04-10` - Minor site re-style: ncdu/ncdc/tuwf now have their own menu.
`2012-03-30` - Updated ncdc-share-report for Go 1 <!- tags: ncdc, link: /dump ->
`2012-03-24` - Moved ncdu bug tracker from sourceforge to this site <!- tags: ncdu, link: /ncdu/bug ->
`2012-03-17` - Wrote a small bug tracker for ncdc <!- tags: ncdc, link: /ncdc/bug ->
`2012-03-14` - ncdc 1.9 released. <!- tags: ncdc, link: /ncdc ->
`2012-02-15` - Added an article on my new communication system. <!- link: /doc/commvis ->
`2012-02-13` - ncdc 1.8 released. <!- tags: ncdc, link: /ncdc ->
`2012-01-19` - TUWF 0.2 released. <!- tags: tuwf, link: /tuwf ->
`2012-01-17` - Complete site redesign.
`2011-12-30` - ncdc 1.7 released! <!- tags: ncdc, link: /ncdc ->
`2011-12-07` - ncdc 1.6 released! <!- tags: ncdc, link: /ncdc ->
`2011-11-26` - Added article section and the article on SQLite. <!- link: /doc ->
`2011-11-03` - ncdu 1.8 released! <!- tags: ncdu, link: /ncdu ->
`2011-11-03` - ncdc 1.5 released! <!- tags: ncdc, link: /ncdc ->
`2011-10-26` - ncdc 1.4 released! <!- tags: ncdc, link: /ncdc ->
`2011-10-19` - PGP-signed all releases of ncdu, ncdc and TUWF. <!- tags: ncdu, ncdc, tuwf ->
`2011-10-14` - ncdc 1.3 released! <!- tags: ncdc, link: /ncdc ->
`2011-09-25` - ncdc 1.1 released - follwed by a 1.2 quickfix. <!- tags: ncdc, link: /ncdc ->
`2011-09-16` - ncdc 1.0 released! <!- tags: ncdc, link: /ncdc ->
`2011-09-15` - Added some screenshots for ncdu. <!- tags: ncdu, link: /ncdu/scr ->
`2011-09-03` - ncdc 0.9 released! <!- tags: ncdc, link: /ncdc ->
`2011-08-26` - ncdc 0.8 released! <!- tags: ncdc, link: /ncdc ->
`2011-08-17` - ncdc 0.7 released! <!- tags: ncdc, link: /ncdc ->
`2011-08-08` - ncdc 0.6 released & user guide updated <!- tags: ncdc, link: /ncdc ->
`2011-08-02` - ncdc 0.5 released! <!- tags: ncdc, link: /ncdc ->
`2011-07-23` - ncdc 0.4 released! <!- tags: ncdc, link: /ncdc ->
`2011-07-15` - ncdc 0.3 released! <!- tags: ncdc, link: /ncdc ->
`2011-06-27` - ncdc 0.2 released! <!- tags: ncdc, link: /ncdc ->
`2011-06-20` - ncdc 0.1 released! And wrote a user guide for it. <!- tags: ncdc, link: /ncdc ->
`2011-06-11` - Added NCurses colour experiment <!- link: /dump/nccolour ->
`2011-06-03` - Added my latest project: ncdc <!- tags: ncdc, link: /ncdc ->
`2011-02-07` - TUWF 0.1 released and now also available on CPAN <!- tags: tuwf, link: /tuwf ->
`2011-01-27` - Documented and uploaded one of my older projects: TUWF <!- tags: tuwf, link: /tuwf ->
`2011-01-09` - Added my json.mll OCaml library to code dump <!- link: /dump ->
`2010-08-13` - ncdu 1.7 released! <!- tags: ncdu, link: /ncdu ->
`2009-12-22` - Added vinfo.c script to code dump <!- link: /dump ->
`2009-10-23` - ncdu 1.6 released! <!- tags: ncdu, link: /ncdu ->
`2009-09-21` - Tiny CSS fix to make this site look good in certain configurations.
`2009-05-02` - ncdu 1.5 released! <!- tags: ncdu, link: /ncdu ->
`2009-04-30` - Site redesign and reorganisation.
-->
