% NCurses Direct Connect

Ncdc is a lightweight direct connect client with a friendly ncurses interface.

## Get ncdc! <a href="/ncdc/feed.atom"><img src="/img/feed_icon.png" alt="Atom feed"></a>

Latest version
:   1.25 (2025-03-04 - [ncdc-1.25.tar.gz](/download/ncdc-1.25.tar.gz)
    \- [changes](https://dev.yorhel.nl/ncdc/changes))

    Convenient static binaries for Linux:
    [x86-64](/download/ncdc-linux-x86_64-1.25.tar.gz) -
    [i486](/download/ncdc-linux-i486-1.25.tar.gz) -
    [ARM](/download/ncdc-linux-arm-1.25.tar.gz) -
    [AArch64](/download/ncdc-linux-aarch64-1.25.tar.gz).  Check the
    [installation instructions](/ncdc/install) for more info.

Development version
:   The latest development version is available from git and can be cloned using
    `git clone git://g.blicky.net/ncdc.git`. The repository is available for
    [online browsing](https://g.blicky.net/ncdc.git/).

Requirements
:   The following libraries are required: ncurses, zlib, bzip2, sqlite3, glib2 and
    gnutls.

    Ncdc is entirely written in C and available under a liberal MIT license.

Community
:   - [Bug tracker](https://code.blicky.net/yorhel/ncdc/issues) - For bugs reports, feature requests and patches.
    - `adcs://dc.blicky.net:2780/` - For real-time chat.<br>
      <small>TLS fingerprint as of 2024-10-04 is 5ZFSEGG26HUYZ7NXD6BARU6JHGLF7WNCP3LNYEFACMPRDFJKUEVA.</small>

Project Status
:   Passive maintenance. No active development has been happening for a while,
    but I'm still around to implement fixes and the occasional feature update.

Packages and ports
:   Are available for the following systems:
    [Arch Linux](https://aur.archlinux.org/packages/ncdc/) -
    [Debian](https://packages.debian.org/search?keywords=ncdc&searchon=names&exact=1&suite=all&section=all) -
    [Fedora](https://apps.fedoraproject.org/packages/ncdc/overview/) -
    [FreeBSD](https://www.freebsd.org/cgi/ports.cgi?query=ncdc&stype=all) -
    [Frugalware](http://frugalware.org/packages?srch=ncdc&op=pkg&arch=all&ver=all) -
    [GNU Guix](https://www.gnu.org/software/guix/packages/N/) -
    [Gentoo](http://packages.gentoo.org/package/net-p2p/ncdc) -
    [Homebrew](https://formulae.brew.sh/formula/ncdc) -
    [OpenSUSE](http://packman.links2linux.org/package/ncdc) -
    [Source Mage](http://download.sourcemage.org/grimoire/codex/test/ftp/ncdc/) -
    [Ubuntu](https://packages.ubuntu.com/search?keywords=ncdc&searchon=names&exact=1&suite=all&section=all) -
    [Void Linux](https://voidlinux.org/packages/?arch=x86_64&q=ncdc).

    A convenient installer is available for
    [Android](http://code.ivysaur.me/ncdcinstaller.html) as well.

## Features

Common features all non-ancient DC clients (should) have:

- Connecting to multiple hubs at the same time,
- Support for both ADC and NMDC protocols,
- Chatting and private messaging,
- Browsing the user list of a connected hub,
- Share management and file uploading,
- Connections and download queue management,
- File list browsing,
- TTH-checked, multi-source and segmented file downloading,
- Searching for files,
- Secure hub (adcs:// and nmdcs://) and client connections on both protocols,
- Bandwidth throttling,
- IPv6 support.

And special features not commonly found in other clients:

- Different connection settings for each hub,
- Encrypted UDP messages (ADC SUDP),
- Subdirectory refreshing,
- Nick notification and highlighting in chat windows,
- Trust on First Use for TLS-enabled hubs,
- A single listen port for both TLS and TCP connections,
- Efficient file uploads using sendfile(),
- Large file lists are opened in a background thread,
- Doesn't trash your OS file cache (with the flush\_file\_cache option enabled),
- (Relatively...) low memory usage.

## What doesn't ncdc do?

Since the above list is getting larger and larger every time, it may be more
interesting to list a few features that are (relatively) common in other DC
clients, but which ncdc doesn't do.

- NAT Traversal,
- OP features (e.g. client detection, file list scanning and other useful stuff for OPs),
- SOCKS support.

Of course, there are many more features that could be implemented or improved,
but given development is currently kind of dead, there's a good chance none of
it will happen anytime soon.
