% Ncdc Q&A

# About ncdc

## What about other text-mode clients?

[microdc2](http://corsair626.no-ip.org/microdc/) - A rather nice client, yet
not exactly there. It's limited to connecting to a single hub, hasn't been
updated since 2006, and the readline interface is slightly awkward to use.

[nanodc](http://sourceforge.net/projects/nanodc/) - Can't comment much on this,
except maybe that rocket science is perhaps easier than getting nanodc to
compile.

LDCC - Uses DCTC as backend and an interface based on TurboVision. All
mentioned projects are dead: neither LDCC, DCTC nor TurboVision are seeing any
recent development.

[ShakesPeer](https://github.com/rufuscoder/Shakespeer) - Appears to have a
commandline interface as well. I haven't personally tried it, but have been
told it has many problems of its own. Has not seen any recent development,
either.

## Why did you start from scratch? Why not use the DC++ core?

There are several reasons why I chose not to use code from existing projects,
but the two most important reasons are the following: 1) I am a control freak,
and 2) personal preferences.

**Control freak:** I have no idea how to create an interface to a protocol if I
don't know the overall design and all the tiny details of the actual protocol
I'm working with. And what's a better way to get used to a protocol than by
writing everything yourself? Then there's some other advantages to
reimplementing everything: I get to choose the library dependencies and the
memory/CPU efficiency trade-offs, and I am not limited by an existing
implementation that needs quite a few modifications to achieve what I want.
Most of the "special features not commonly found in other clients" mentioned on
the [homepage](/ncdc) are a direct result of this.

**Personal preferences:** These are simple: I rather dislike C++ and working
with other people's code. Working with other people's C++ code isn't exactly
something I wish to spend my free time on.

## Does ncdc support TLS 1.2?

Yes, but you need a recent version of GnuTLS. Nobody knows what counts as
"recent", exactly, but I'm guessing any 3.0+ version will do.

## Does ncdc support TLS 1.3?

Yes, but you need an even more recent version of GnuTLS.

## What protocol features does ncdc support?

For ADC: BASE, RF, TIGR, BZIP, BLOM, ADCS, KEYP and SUDP.

For NMDC: NoGetINFO, NoHello, UserIP2, MiniSlots, XmlBZList, ADCGet, TTHL and TTHF.

ncdc also supports TLS-enabled connections for both hub connections and
client-to-client connections on both ADC and NMDC protocols. Note that ncdc
does not support some of the older NMDC protocol features, like $Get,
$GetZBlock, $CHUNK, $Cancel or non-XML file lists. I am not aware of an other
up-to-date client that still uses any of these features.

## What are those flags / character indications in the connection list?

Since the manual page doesn't cover those yet, I'll document it here for now:

The header has `St`, where the `S` stands for Status and `t` for whether TLS
encryption is used or not. The status flags can be either **C**onnecting,
**H**andshake, **I**dle, **D**ownloading, **U**ploading or **-** for disconnected.

## ...And what about those in the user list?

The user list has three boolean flags: **O**perator, **P**assive, and whether the client has **T**LS enabled.

# Troubleshooting

## Active mode doesn't work!

It's possible that you are still listed as passive in some hubs even if you
have `/set active true`. This happens when the (NMDC) hub does not support IP
autodetection or when it drops user info updates. You can work around this
issue by manually setting your public IP address with `/set active_ip
YOUR-IP-HERE`.

## The Alt- keys don't work!

The ncdc manual refers to the "meta" key as Alt-something, but the actual key
to use tends to differ depending on your setup. In almost every setup, you can
press and release the 'Esc' key as a replacement for Alt-something. If you're
on OS X, [this stackoverflow answer](http://stackoverflow.com/a/438892)
may be helpful.

## Ncdc uses too much disk space!

First, look where this disk space goes to (hint: use [ncdu](/ncdu)). If it's
the log files: you can safely delete or rotate them (see next question).

The _db.sqlite3_ file can also grow quite large in certain situations. If you
modify or rename a lot of files in your share and ncdc re-hashes them, the old
hash data associated with the files is not removed from the database, resulting
in wasted disk space. The `/gc` command in ncdc can be used to clean up this
unused data. Be warned, however, that this command needs roughly twice the size
of the old db.sqlite3 file for temporary storage, so make sure you have enough
space available. (Note that this behaviour is not specific to ncdc, most other
DC clients do the same.)

## Why doesn't ncdc rotate log files automatically?

Because you can easily do that yourself. You can either use logrotate or a
simple script that runs from a cron. For an example of the latter option,
[this is the script I use](http://p.blicky.net/s7132), which is run as a
monthly cron job.

## "TLS error: Key usage violation in certificate has been detected."

Some hub operators don't know how to generate proper TLS certificates and other
DC clients tend to be even more lax about validating certificates than ncdc. To
work around this error, add the `%COMPAT` flag to your `tls_priority`, for
example: `/set tls_priority NORMAL:%COMPAT`.


# Can ncdc...

## Can ncdc run in the background / as a daemon?

As with most ncurses applications: no. At least, it does not have this
functionality built-in. Ncdc is designed to be used in combination with a
separate terminal multiplexer or detach utility to handle this. Have a look at
[GNU screen](http://www.gnu.org/s/screen/),
[tmux](https://github.com/tmux/tmux) or [dtach](http://dtach.sourceforge.net/).

## Does ncdc support UPnP?

Not natively. However, it is possible to use [this
script](http://www.howtoforge.com/administrating-your-gateway-device-via-upnp)
and manually keep a port open using a cron job. I have no experience with this
myself, though. I just run ncdc directly on my router. :-)

## Are there any programs available for analyzing the transfers.log file?

Nothing like that is included in the release yet, but there is a simple Perl
script available: [ncdc-transfer-stats](http://p.blicky.net/eu00a), and a short
Go program: [ncdc-share-report](http://p.blicky.net/h25z8).

## Can ncdc use the hash data or configuration from an existing DC++ installation?

No, ncdc uses its own configuration and hash storage directory.  However, on
popular demand I could write a conversion utility to transfer the hash data
from other clients to ncdc's format. (Contrary to my expectations, there hasn't
been much interest in such a tool ever since I wrote this FAQ entry many years
ago.  So I guess this isn't really a FAQ).
