% Ncdc Installation Instructions

# General instructions

## Building from source

In theory, the following instructions should work everywhere:

- Install the required dependencies: ncurses, bzip2, zlib, sqlite3, glib2 and gnutls,
- Download and extract the source tarball from the [homepage](/ncdc),
- `./configure`
- `make`
- And then run `make install` with superuser permissions.

In practice, however, this does not always work and may not always be the
prefered method of installation. On this page I try to collect instructions for
each OS and distribution to make the installation process a bit easier for
everyone.

If your system is missing from this page or if you're still having trouble,
don't hesitate to join the support hub at `adcs://dc.blicky.net:2780/` or send
me a mail at [projects@yorhel.nl](mailto:projects@yorhel.nl). Contributions to
this page are of course highly welcomed as well. :-)

## Statically linked binaries

If you just want to get ncdc running without going through the trouble of
compiling and/or installing it, I also offer statically linked binaries:

- [Linux, x86-64](/download/ncdc-linux-x86_64-1.25.tar.gz)
- [Linux, i486](/download/ncdc-linux-i486-1.25.tar.gz)
- [Linux, ARM](/download/ncdc-linux-arm-1.25.tar.gz)
- [Linux, AArch64](/download/ncdc-linux-aarch64-1.25.tar.gz)

To use them, simply download and extract the tarball, and then run `./ncdc` on
the command line.

The binaries include all the required dependencies and are linked against
[musl](http://www.etalabs.net/musl/), so they should run on any Linux machine
with the right architecture. If you want binaries for an other OS or
architecture, please bug me and I'll see what I can do.

# System-specific instructions

(These instructions have not been tested in a long time, there's a good chance
they may not work anymore)

## Android

An [convenient installer](http://code.ivysaur.me/ncdcinstaller.html) is
available for Android 2.3 and later, which makes use of the static binary.

## Arch Linux

Ncdc is available on [AUR](https://aur.archlinux.org/packages/ncdc/), to
install it you can use your favorite AUR-installer. If you don't have a
favorite, go for the manual approach:

    wget https://aur.archlinux.org/cgit/aur.git/snapshot/ncdc.tar.gz
    tar -xf ncdc.tar.gz
    cd ncdc
    makepkg -si

## Fedora

There's a [package](https://apps.fedoraproject.org/packages/ncdc/overview/)
available for Fedora.

## FreeBSD

Ncdc is available in the Ports Collection. To install, [make sure your
collection is
up-to-date](http://www.freebsd.org/doc/en_US.ISO8859-1/books/handbook/ports-using.html)
and install the Port as any other:

    cd /usr/ports/net-p2p/ncdc
    make install clean

## Gentoo

Ncdc is available in the Portage tree, so installation is trivial:

    emerge ncdc

## Mac OS X

Ncdc is available in [Homebrew](https://formulae.brew.sh/formula/ncdc).

## OpenBSD

Compile & install from source:

    doas pkg_add -i glib2
    ftp https://dev.yorhel.nl/download/ncdc-1.25.tar.gz
    tar zxvf ncdc-1.25.tar.gz
    cd ncdc-1.25
    ./configure NCURSES_CFLAGS="-lncurses" NCURSES_LIBS="-lncursesw"\
        CPPFLAGS="-I/usr/include -I/usr/local/include"\
        LDFLAGS="-L/usr/lib -L/usr/local/lib"
    make
    doas make install

## OpenIndiana

This has been tested on OpenIndiana Build 151a Server, but may work on other
versions as well. Compiling from source is your only option at the moment.
First install some required packages (as root):

    pkg install gcc-3 glib2 gnutls gettext header-math perl-510/extra

Then, fetch the ncdc source tarball, extract and build as follows:

    wget https://dev.yorhel.nl/download/ncdc-1.25.tar.gz
    tar -xf ncdc-1.25.tar.gz
    cd ncdc-1.25
    export PATH="$PATH:/usr/perl5/5.10.0/bin"
    ./configure --prefix=/usr LDFLAGS='-L/usr/gnu/lib -R/usr/gnu/lib'
    make

And finally, to actually install ncdc, run `make install` as root. You can
safely revert `$PATH` back to its previous value if you wish, it was only
necessary in order for `./configure` and `make` to find `pod2man`.

## OpenSUSE

Get the package from [PackMan](http://packman.links2linux.org/package/ncdc):
Select your openSUSE release and hit the "1 click install" button.

## Ubuntu & Debian

If you're running a recent enough release, you may be able to just do a

    sudo apt install ncdc

Otherwise you may have to grab the package from a backport or use the static
binaries above.

Alternatively, you can also try to compile ncdc from source. To do so, first
install the required libraries:

    sudo apt-get install libbz2-dev libsqlite3-dev libncurses5-dev\
      libncursesw5-dev libglib2.0-dev libgnutls-dev zlib1g-dev

Then run the following commands to download and install ncdc:

    wget https://dev.yorhel.nl/download/ncdc-1.25.tar.gz
    tar -xf ncdc-1.25.tar.gz
    cd ncdc-1.25
    ./configure --prefix=/usr
    make
    sudo make install

## Windows (Cygwin)

Surprisingly enough, ncdc can be used even on Windows, thanks to Cygwin.  If
you haven't done so already, get `setup.exe` from the [Cygwin
website](http://cygwin.com/) and use it to install the following packages:

- make
- gcc4
- perl
- pkg-config
- wget
- zlib-devel
- libncursesw-devel
- libbz2-devel
- libglib2.0-devel
- libsqlite3-devel
- gnutls-devel

Then open a Cygwin terminal and run the following commands to download,
compile, and install ncdc:

    wget https://dev.yorhel.nl/download/ncdc-1.25.tar.gz
    tar -xf ncdc-1.25.tar.gz
    cd ncdc-1.25
    ./configure --prefix=/usr
    make install
