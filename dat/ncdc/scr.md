% Ncdc Screenshots

Note: While these screenshots are from version 1.5, the latest version has
little visible changes.

## Main chat

![Ncdc in the mainchat.](/img/ncdchub.png)

## File browser

![Simple file list browser.](/img/ncdcbrowse.png)

## User list

![Ncdc displaying the userlist of a hub.](/img/ncdcusers.png)

## Built-in help

![Ncdc built-in help.](/img/ncdchelp.png)
