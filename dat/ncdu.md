% NCurses Disk Usage

Ncdu is a disk usage analyzer with a text-mode user interface. It is designed
to find space hogs on a remote server where you don't have an entire graphical
setup available, but it is a useful tool even on regular desktop systems. Ncdu
aims to be fast, simple, easy to use, and should be able to run on any
POSIX-like system.

## Notable updates

Parallel scanning
:   Ncdu 2.5 adds support for parallel scanning, but it's not enabled by
    default. To give it a try, run with `-t8` to scan with 8 threads.[^1]

Binary export
:   Ncdu 2.6 adds a new binary export format that better works with parallel
    scanning, offers built-in compression and supports browsing directory
    trees that are too large to fit in memory. To give it a try, use the `-O`
    flag instead of `-o`.

Colors
:   Ncdu has had color support since version 1.13. Colors were enabled by
    default in 1.17 and 2.0, and then later disabled again in 1.20 and 2.4
    because the text was not legible in all terminal configurations. If you do
    prefer the colors, add `--color=dark` to your [config
    file](/ncdu/man#configuration). I hope to add more flexible color support
    at some point.

## Download <a href="/ncdu/feed.atom"><img src="/img/feed_icon.png" alt="Atom feed"></a>

Static binaries
:   Convenient static binaries for Linux. Download, extract and run; no
    compilation or installation necessary:
    [x86](/download/ncdu-2.8-linux-x86.tar.gz) -
    [x86_64](/download/ncdu-2.8-linux-x86_64.tar.gz) -
    [ARM](/download/ncdu-2.8-linux-arm.tar.gz) -
    [AArch64](/download/ncdu-2.8-linux-aarch64.tar.gz).

Zig version (stable)
:   2.8 (2025-03-05 - [ncdu-2.8.tar.gz](/download/ncdu-2.8.tar.gz) - [changes](/ncdu/changes2))

    Requires Zig 0.14.

    The [Zig language](https://ziglang.org/) and compiler are still somewhat
    unstable, use the ncdu 1.x branch if this does not work for you or if you
    need a more stable compilation environment.

C version (LTS)
:   1.22 (2025-03-05 - [ncdu-1.22.tar.gz](/download/ncdu-1.22.tar.gz) - [changes](/ncdu/changes))

    Lags a bit behind on the 2.x version in terms of features and performance,
    but is still being maintained and perfectly usable.

Development version
:   The most recent code is available on git:
    ```
    git clone git://g.blicky.net/ncdu.git/
    ```
    The repository is also available for online browsing on
    [Forgejo](https://code.blicky.net/yorhel/ncdu/) and
    [cgit](https://g.blicky.net/ncdu.git/).
    The 'master' branch represents the C version, the Zig version can be found
    in the 'zig' branch.

License
:   MIT.

## Packages and ports

Ncdu has been packaged for quite a few systems, here's a list of the ones I am aware of:

[AIX](http://www.perzl.org/aix/index.php?n=Main.Ncdu) -
[Alpine Linux](http://pkgs.alpinelinux.org/packages?name=ncdu) ([2](https://pkgs.alpinelinux.org/packages?name=ncdu2&branch=edge&repo=&arch=&maintainer=)) -
[ALT Linux](https://packages.altlinux.org/en/sisyphus/srpms/ncdu/) -
[Arch Linux](https://www.archlinux.org/packages/?q=ncdu) -
[CRUX](https://crux.nu/portdb/?q=ncdu&a=search) -
[Cygwin](https://cygwin.com/cgi-bin2/package-grep.cgi?grep=ncdu) -
[Debian](http://packages.debian.org/ncdu) -
[Fedora](https://packages.fedoraproject.org/pkgs/ncdu/ncdu/) -
[FreeBSD](https://www.freebsd.org/cgi/ports.cgi?query=ncdu&stype=all) -
[Frugalware](https://frugalware.org/package/ncdu/) -
[Gentoo](https://packages.gentoo.org/packages/sys-fs/ncdu) ([-bin](https://packages.gentoo.org/packages/sys-fs/ncdu-bin)) -
[GNU Guix](https://guix.gnu.org/packages/N/) -
[NixOS](https://github.com/NixOS/nixpkgs/blob/master/pkgs/tools/misc/ncdu/default.nix) -
[OpenBSD](https://cvsweb.openbsd.org/cgi-bin/cvsweb/ports/sysutils/ncdu/) -
[openSUSE](https://software.opensuse.org//download.html?project=utilities&package=ncdu) -
[OpenWRT](https://openwrt.org/packages/pkgdata/ncdu) -
MacOS ([Fink](https://pdb.finkproject.org/pdb/package.php/ncdu) - [Homebrew](https://formulae.brew.sh/formula/ncdu) - [MacPorts](https://ports.macports.org/search/?q=ncdu&name=on)) -
[Solaris](https://www.opencsw.org/packages/ncdu) -
[Slackware](https://slackbuilds.org/repository/15.0/system/ncdu/) -
[Ubuntu](https://packages.ubuntu.com/search?searchon=sourcenames&keywords=ncdu) -
[Void Linux](https://voidlinux.org/packages/?arch=x86_64&q=ncdu).

Packages for NetBSD, DragonFlyBSD, MirBSD and others can be found on
[pkgsrc](http://pkgsrc.se/sysutils/ncdu).

## Similar projects

There's no shortage of alternatives to ncdu nowadays. In no particular order:

- [Duc](http://duc.zevv.nl/) - Multiple user interfaces, C, scales beyond directories that fit in RAM.
- [gt5](http://gt5.sourceforge.net/) - TUI/HTML, also supports diffing.
- [gdu](https://github.com/dundee/gdu) - TUI/CLI, Go, supports ncdu JSON export and import.
- [dua](https://github.com/Byron/dua-cli) - CLI, Rust.
- [pdu](https://github.com/KSXGitHub/parallel-disk-usage) - CLI, Rust.
- [diskonaut](https://github.com/imsnif/diskonaut) - TUI, Rust, treemap.
- [dut](https://codeberg.org/201984/dut), CLI, C.
- [godu](https://github.com/viktomas/godu) - TUI, Go, slightly different browser UI.
- [tdu](https://github.com/josephpaul0/tdu) - CLI, Go, supports ncdu JSON export.
- [TreeSize](http://treesize.sourceforge.net/) - GTK, using a treeview.
- [Baobab](http://www.marzocca.net/linux/baobab/) - GTK, using pie-charts, a treeview and a treemap. Comes with GNOME.
- [GdMap](http://gdmap.sourceforge.net/) - GTK, treemap.
- [Filelight](https://apps.kde.org/filelight/) - KDE, using pie-charts.
- [QDirStat](https://github.com/shundhammer/qdirstat) - Qt, treemap.
- [K4DirStat](https://github.com/jeromerobert/k4dirstat) - Qt, treemap.
- [xdiskusage](http://xdiskusage.sourceforge.net/) - FLTK, with a treemap display.
- [fsv](http://fsv.sourceforge.net/) - 3D visualization.

[^1]: If you're running an unusual setup, such as networked storage, odd
    filesystems, complex RAID configurations, etc, I'd love to hear about the
    performance impact of this new feature. To run benchmarks, `-0
    --quit-after-scan` can be useful to disable the browser interface, or run
    with `-0o/dev/null` to benchmark JSON export. Feedback is welcome on the
    [issue tracker](https://code.blicky.net/yorhel/ncdu/issues) or to
    [projects@yorhel.nl](mailto:projects@yorhel.nl).
