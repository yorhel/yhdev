% Ncdu Screenshots

These screenshots were made with ncdu 1.13 with the `--color=dark` option.

## Scanning...

![Ncdu scanning a large directory.](/img/ncduscan-2.png)

## Done scanning

![Ncdu done scanning a large directory.](/img/ncdudone-2.png)

## Directory information

![Ncdu displaying directory information.](/img/ncduinfo-2.png)

## Delete confirmation

![Ncdu asking for confirmation to delete a file.](/img/ncduconfirm-2.png)

## Help screen

![Ncdu help screen.](/img/ncduhelp1-2.png)

## About screen

![Ncdu about screen.](/img/ncduhelp2-2.png)
