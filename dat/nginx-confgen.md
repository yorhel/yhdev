% Nginx Configuration Generator

nginx-confgen is a simple preprocessor and macro system for
[nginx](http://nginx.org/) and nginx-like configuration files. It support
variable substitution, macro expansion and using the output of arbitrary
commands to generate config files.

## Example

```perl
pre_set $certdir /etc/nginx-certificates;

# Fetch the 'resolver' from /etc/resolv.conf
pre_exec $nameserver "grep nameserver /etc/resolv.conf \\
                      | head -n 1 | sed 's/^nameserver //'";
resolver $nameserver;

# Convenient macro to create a HTTPS virtual host
macro vhost $domain @aliases &block {
  server {
    listen [::]:443 ssl;
    server_name $domain @aliases;

    ssl_certificate     $certdir/$domain/fullchain.pem;
    ssl_certificate_key $certdir/$domain/privkey.pem;
    pre_if -f $certdir/$domain/ocsp.der {
      ssl_stapling_file $certdir/$domain/ocsp.der;
    }

    &block;
  }
}

vhost example.com www.example.com {
  root /var/www/example.com;
}
```

See the [manual](/nginx-confgen/man) for more features.

## Download

Latest version <a href="/nginx-confgen/feed.atom"><img src="/img/feed_icon.png" alt="Atom feed"></a>
:   2.2 (2024-10-22 - [nginx-confgen-2.2.tar.gz](/download/nginx-confgen-2.2.tar.gz) - [changes](/nginx-confgen/changes))

Requirements
:   A C compiler and a UNIX-like system. Only tested on Linux, but should also
    work on \*BSD and MacOS (patches welcome if this is not the case).

License
:   MIT

Distribution packages
:   nginx-confgen is available in [Debian](https://packages.debian.org/nginx-confgen).

Development version
:   `git clone https://code.blicky.net/yorhel/nginx-confgen.git`

    The git repository is also available for [online browsing](https://code.blicky.net/yorhel/nginx-confgen).
