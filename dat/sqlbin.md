% SQLBin

SQLBin is a minimalistic web application that lets you query a
[PostgreSQL](https://www.postgresql.org/) database and save and share the
queries with others.  It's something of a mix between a
[pastebin](https://en.wikipedia.org/wiki/Pastebin) and a database querying
tool.

## Features

- Simple, fast and minimalistic UI
- Efficiently handles and caches large query results
- Integrates with [gnuplot](http://gnuplot.info/) to visualize query results
- Automatically generates documentation for your schema
- Supports saving, tagging, sharing and browsing queries
- Export query results to TSV, SQL, CSV or JSON
- Supports multiple users
- Built-in syntax highlighting (provided by [Prism](https://prismjs.com/))
- Keeps track of queries that broke after a schema update
- Single-binary deployment^[Or, well, two binaries if you include the custom
  build of gnuplot.]
- Fully usable without JavaScript^[JavaScript is only used for optional
  syntax highlighting.]

## Download

Requirements
:   Linux. Other OSes may also work if you don't enable gnuplot support or
    disable the seccomp patch, but I can't really recommend that.

Latest release
:   There's no numbered releases or static binaries yet, get in touch if this
    interests you.

Development version
:   The most recent code is available on [a git
    repository](https://code.blicky.net/yorhel/sqlbin).

License
:   AGPL-3.0-only.

## Similar projects

SQLBin is heavily inspired by [SQLPad](https://getsqlpad.com/en/introduction/).
My main motivation for writing SQLBin was to provide a less heavyweight
alternative with an improved query browser and better intergration with
PostgreSQL.

There's a whole list of alternative database querying tools [over
here](https://github.com/mgramin/awesome-db-tools#gui).
