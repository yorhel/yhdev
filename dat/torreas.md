% Torreas - Torrent file reassembler

Torreas is a small command-line utility to scan directories in order to find
files that are mentioned in a given torrent.  It does this by only comparing
file sizes and checksums, not location or file name, so it can be used to
reconstruct a torrent that you have already downloaded, renamed and archived.

## Download

Latest version <a href="/torreas/feed.atom"><img src="/img/feed_icon.png" alt="Atom feed"></a>
:   0.3 (2024-04-21 - [torreas-0.3.tar.gz](/download/torreas-0.3.tar.gz) - [changes](/torreas/changes))

Build requirements
:   Zig 0.12 or 0.13

License
:   MIT

Development version
:   Available [on git](https://code.blicky.net/yorhel/torreas).

## Bugs & limitations

Torreas currently does an **exhaustive search** and is therefore able to find
*all* matching (duplicate) files. This is usually overkill, as you'll only need
one match for each file to reassemble a torrent. This totally hasn't been a
problem in my tests so far, but if there's many candidate files it may be
faster to stop searching after the first match. I may or may not fix this in
the future.

**Padding files** in torrents are not yet supported. Should be easy to fix, I
simply haven't encountered such a torrent yet.

Torreas could be a lot faster if it could use multiple cores to verify
checksums, but the current version is **single-threaded**. I don't expect this
to change anytime soon, I don't particularly enjoy writing threaded code and
Zig, in its current form, doesn't yet have the tools to help out much in this
area either.

Because torrent checksums can span multiple files, there are edge cases where
torreas will run out of memory or simply never complete while it is trying to
brute-force all possible file combinations. I may add more heuristics or manual
user feedback in the future to better handle such cases.
