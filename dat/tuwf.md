% The Ultimate Website Framework

TUWF is a very small and lightweight web development framework for Perl. It has
evolved from being a few abstraction layers in two large websites to a separate
set of modules. While initially designed to be used for large and complex
websites, it is also perfectly suited for small single-file websites.

## Main features

- Very small, and no extra modules required for the base functionality,
- Easy built-in routing,
- Handy form validation functions,
- Easy XML/HTML generation,
- Response buffering and output compression,
- Easy access to request data,
- Support for CGI, FastCGI and a built-in web server,
- Uses UTF-8 for all text,
- Convenient SQL execution functions and correct transaction handling,
- Open source (duh!) and available under a liberal MIT license.

Read the [description](/tuwf/man#description) in the documentation for more
information and details.

## Download <a href="/tuwf/feed.atom"><img src="/img/feed_icon.png" alt="Atom feed"></a>

**Latest packaged version:** 1.5 ([TUWF-1.5.tar.gz](/download/TUWF-1.5.tar.gz)
\- [CPAN mirror](https://metacpan.org/release/TUWF))

TUWF is also available on a git repository at
[https://code.blicky.net/yorhel/tuwf](https://code.blicky.net/yorhel/tuwf).

## Websites using TUWF

(Not a whole lot)

- [VNDB.org](https://vndb.org/) (the site that spawned TUWF - [open source](https://g.blicky.net/vndb.git/))
- [Manned.org](https://manned.org/) ([open source](https://g.blicky.net/manned.git/))
- [Blicky.net Pastebin](https://p.blicky.net/) ([open source](https://g.blicky.net/bpaste.git/tree/index.cgi))
- The website embedded in the [D&R Axum](http://www.d-r.nl/axum.html) mixing console.
- [333networks](http://333networks.com/)
