#!/usr/bin/perl

($project) = (shift =~ /^([^ \/]+)/);
$project = uc $project if $project eq 'tuwf';
$title = shift;

print "---\ntitle: $title\npage-type: changelog\n...\n";

for (split /\n\n/, join '', grep !/^#/, <>) {
    s/^\s*//;
    s/^([^\s]+)\s+-\s+([0-9]{4}-[0-9]{2}-[0-9]{2})//;
    print "\n-   **$1** - $2";
    $dl = "$project-$1.tar.gz";
    $dl = "$project-linux-amd64-$1.tar.gz" if !-f "pub/download/$dl";
    print " - [$dl](/download/$dl)" if -f "pub/download/$dl";
    print "\n";

    for (split /\r?\n\s+-\s+/) {
        s/([*_\\])/\\$1/g if !/`/;
        s/--/\\--/g if !/`/;
        print "    - $_\n" if $_;
    }
}
