#!/usr/bin/perl

use v5.26;
use CGI::Minimal;

die if !$ENV{CONTENT_LENGTH};
CGI::Minimal::max_read_size(128*1024);
my $cgi = CGI::Minimal->new;
die if $cgi->truncated;

my $msg = $cgi->param('message');
my $spam = $msg =~ /<a href/ || $msg =~ /^katana/;

if (!$spam) {
    open my $S, '|-', qw'/usr/sbin/sendmail -t -f dev@yorhel.nl' or die $!;
    printf $S <<'_', $cgi->param('name'), $msg;
To: dev@yorhel.nl
From: dev@yorhel.nl
Subject: Guestbook post
Content-Type: text/plain; charset='UTF-8'

Name: %s

Message:

%s
_
    close $S or die;
}

print <<'_';
Status: 303
Content-Type: text/plain
Location: https://dev.yorhel.nl/guestbook/thanks

Redirecting...
_
