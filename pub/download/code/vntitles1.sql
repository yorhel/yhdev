BEGIN;

-- The original VIEW solution for comparison:
CREATE OR REPLACE VIEW vnt AS
    SELECT v.*
         , COALESCE(ve.title, vo.latin, vo.title) AS title
         , CASE WHEN vo.latin IS NULL THEN '' ELSE vo.title END AS alttitle
     FROM vn v
     JOIN vn_titles vo ON vo.id = v.id AND vo.lang = v.olang
     LEFT JOIN vn_titles ve ON ve.id = v.id AND ve.lang = 'en';


CREATE TABLE vnt_type (LIKE vn, title text, alttitle text);

CREATE TYPE langprefs AS (
    t1_lang     language, -- NULL for original language
    t2_lang     language,
    t3_lang     language,
    t4_lang     language,
    t5_lang     language,
    a1_lang     language,
    a2_lang     language,
    a3_lang     language,
    a4_lang     language,
    a5_lang     language,
    t1_latin    boolean,
    t2_latin    boolean,
    t3_latin    boolean,
    t4_latin    boolean,
    t5_latin    boolean,
    a1_latin    boolean,
    a2_latin    boolean,
    a3_latin    boolean,
    a4_latin    boolean,
    a5_latin    boolean,
    t1_official boolean,  -- always true for original language
    t2_official boolean,
    t3_official boolean,
    t4_official boolean,
    t5_official boolean,
    a1_official boolean,
    a2_official boolean,
    a3_official boolean,
    a4_official boolean,
    a5_official boolean
);

CREATE OR REPLACE FUNCTION vnt(p langprefs) RETURNS SETOF vnt_type AS $$
    SELECT v.*
         , coalesce(
                CASE WHEN p.t1_latin THEN t1.latin ELSE NULL END, t1.title,
                CASE WHEN p.t2_latin THEN t2.latin ELSE NULL END, t2.title,
                CASE WHEN p.t3_latin THEN t3.latin ELSE NULL END, t3.title,
                CASE WHEN p.t4_latin THEN t4.latin ELSE NULL END, t4.title,
                CASE WHEN p.t5_latin THEN t5.latin ELSE NULL END, t5.title
           ) AS title
         , coalesce(
                CASE WHEN p.a1_latin THEN a1.latin ELSE NULL END, a1.title,
                CASE WHEN p.a2_latin THEN a2.latin ELSE NULL END, a2.title,
                CASE WHEN p.a3_latin THEN a3.latin ELSE NULL END, a3.title,
                CASE WHEN p.a4_latin THEN a4.latin ELSE NULL END, a4.title,
                CASE WHEN p.a5_latin THEN a5.latin ELSE NULL END, a5.title
           ) AS alttitle
      FROM vn v
      LEFT JOIN vn_titles t1 ON t1.id = v.id AND t1.lang = COALESCE(p.t1_lang, v.olang) AND (NOT p.t1_official OR t1.official)
      LEFT JOIN vn_titles t2 ON t2.id = v.id AND t2.lang = COALESCE(p.t2_lang, v.olang) AND (NOT p.t2_official OR t2.official) AND p.t1_lang IS NOT NULL
      LEFT JOIN vn_titles t3 ON t3.id = v.id AND t3.lang = COALESCE(p.t3_lang, v.olang) AND (NOT p.t3_official OR t3.official) AND p.t1_lang IS NOT NULL AND p.t2_lang IS NOT NULL
      LEFT JOIN vn_titles t4 ON t4.id = v.id AND t4.lang = COALESCE(p.t4_lang, v.olang) AND (NOT p.t4_official OR t4.official) AND p.t1_lang IS NOT NULL AND p.t2_lang IS NOT NULL AND p.t3_lang IS NOT NULL
      LEFT JOIN vn_titles t5 ON t5.id = v.id AND t5.lang = COALESCE(p.t5_lang, v.olang) AND (NOT p.t5_official OR t5.official) AND p.t1_lang IS NOT NULL AND p.t2_lang IS NOT NULL AND p.t3_lang IS NOT NULL AND p.t4_lang IS NOT NULL
      LEFT JOIN vn_titles a1 ON a1.id = v.id AND a1.lang = COALESCE(p.a1_lang, v.olang) AND (NOT p.a1_official OR a1.official)
      LEFT JOIN vn_titles a2 ON a2.id = v.id AND a2.lang = COALESCE(p.a2_lang, v.olang) AND (NOT p.a2_official OR a2.official) AND p.a1_lang IS NOT NULL
      LEFT JOIN vn_titles a3 ON a3.id = v.id AND a3.lang = COALESCE(p.a3_lang, v.olang) AND (NOT p.a3_official OR a3.official) AND p.a1_lang IS NOT NULL AND p.a2_lang IS NOT NULL
      LEFT JOIN vn_titles a4 ON a4.id = v.id AND a4.lang = COALESCE(p.a4_lang, v.olang) AND (NOT p.a4_official OR a4.official) AND p.a1_lang IS NOT NULL AND p.a2_lang IS NOT NULL AND p.a3_lang IS NOT NULL
      LEFT JOIN vn_titles a5 ON a5.id = v.id AND a5.lang = COALESCE(p.a5_lang, v.olang) AND (NOT p.a5_official OR a5.official) AND p.a1_lang IS NOT NULL AND p.a2_lang IS NOT NULL AND p.a3_lang IS NOT NULL AND p.a4_lang IS NOT NULL
$$ LANGUAGE SQL STABLE;


SET max_parallel_workers_per_gather = 0;

EXPLAIN ANALYZE
SELECT id, title, alttitle
  --FROM vnt('(en,,,,,,,,,,t,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f)'::langprefs) x
  FROM vnt
 WHERE NOT hidden --AND x.id < 'v10';
 ORDER BY title LIMIT 100;

EXPLAIN ANALYZE
SELECT id, title, alttitle
  --FROM vnt('(en,,,,,,,,,,t,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f)'::langprefs) x
  FROM vnt
 WHERE NOT hidden --AND x.id < 'v10';
 ORDER BY title LIMIT 100;
